import { feApiStatus, feApiError } from '../lib/util';
import { Router } from 'express';
import PlatformFactory from '../platform/factory';


export default ({ config, db }) => {
    let newsletterApi = Router();

    const _getProxy = (req) => {
        const platform = config.platform
        const factory = new PlatformFactory(config, req)
        return factory.getAdapter(platform, 'newsletter')
      };


    /*POST Newsletter Subscribe */
    newsletterApi.post('/subscribe', (req, res) => {
        try {
            console.log("subscribe", req.body)
            const newsletterProxy = _getProxy(req)
            console.log("newsletterProxy",newsletterProxy)
            newsletterProxy.subscribe(req.body).then((result) => {
                feApiStatus(res, result, 200);
            }).catch(err => {
                feApiError(res, err);
            })
        } catch (err) {
            feApiError(res, err);
        }
    });

    return newsletterApi
}
