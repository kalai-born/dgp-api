
import { Layout } from "./implementations/layout";
import * as express from 'express'

export interface ILayoutRepo {
  getPageLayout (path:Array<string>,storeCode: string,field_name:string,pageId:any): Promise<Layout>;  
}
