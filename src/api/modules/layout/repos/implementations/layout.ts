
import { ILayoutRepo } from "../layout";
import express from "express";
import { getPath } from "src/lib/util";
import { dataService } from "../../../shared/Services/ElasticSearch"

export class Layout implements ILayoutRepo {

  async getPageLayout (path:Array<string>,storeCode: string,field_name: string,pageId:any): Promise<any> {        
        let field_map = new Map()
        field_map.set(field_name,pageId)
        return dataService.getData(storeCode,path,field_map) 
        //return await layout.getLayout(path,storeCode,fields)
  }
}