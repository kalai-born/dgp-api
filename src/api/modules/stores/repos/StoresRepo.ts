import express from "express";

export interface IStoresRepo {
  getStores(req:express.Request,storeCode: string, field_name:string,storeSearch:string): Promise<any>;
}
