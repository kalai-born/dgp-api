import { IStoresRepo } from "../StoresRepo"
import express from "express";
import { dataService } from "../../../shared/Services/ElasticSearch";
import { getPath } from "../../../../../lib/util";

export class StoresRepo implements IStoresRepo {

  async getStores(req: express.Request, storeCode: string, field_name: string, storeSearch: string): Promise<any> {
    let path = getPath(req, 'getStores');
    let field_map = new Map()
    field_map.set(field_name, storeSearch)
    return dataService.getData(storeCode, path, field_map)
  }
}