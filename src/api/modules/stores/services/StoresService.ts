import { convertToArray, getFormattedResults } from '../../../../lib/util'
import { Service } from '../../shared/core/Service'
import { IStoresRepo } from '../repos/StoresRepo';
import { Either, Result, left, right } from "../../shared/core/Result"
import { AppError } from "../../shared/core/AppError";
import { Request } from 'express-serve-static-core';
import express from 'express';
import config from 'config'

const R = require('ramda');
const DEFAULT_FIELD = 'field_store_code';
const FIELD_SEARCH = 'STORE*';

type Response = Either<
    AppError.UnexpectedError,
    Result<any>
>
export class StoresService implements Service<Request, Promise<any>> {
    private storesRepo: IStoresRepo;

    constructor(storesRepo: IStoresRepo) {
        this.storesRepo = storesRepo;
    }

    async execute(request: express.Request): Promise<any> {
        try {
            const storeCode = request.query.storeCode.toString();
            let items = await this.storesRepo.getStores(request,storeCode,DEFAULT_FIELD,FIELD_SEARCH)   
            let storeData = [];
            for (let i = 0; i < items.length; i++) {
              let data = (items.length > 0 && items[i].field_nodejson) ? JSON.parse(items[i].field_nodejson) : {};
              storeData.push(data);
            }


            return right(Result.ok<any>({storeData}));
        } catch (error) {
            return left(new AppError.UnexpectedError(error));
        }
    }
}