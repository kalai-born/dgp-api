
import express from 'express'
import { getStoresController } from '../controllers';


const stores = express.Router();
let config: any
let db: any
export default ({ config, db }) => {
  stores.get('/getStores',
    (req, res) => getStoresController.execute(req, res))
  return stores;
}

