
import * as express from 'express'
import { BaseController } from "../../shared/infra/http/models/BaseController";
import { StoresService } from '../services/StoresService';
import { StoresError } from '../errors/StoresError';

export class StoresController extends BaseController {
    private useCase: StoresService;

    constructor(useCase: StoresService) {
        super();
        this.useCase = useCase;
    }

    async executeImpl(req: express.Request, res: express.Response): Promise<any> {

        try {
            const result = await this.useCase.execute(req);

            if (result.isLeft()) {
                const error = result.value;

                switch (error.constructor) {
                    case StoresError.Exception:
                        return this.notFound(res, error.errorValue().message)
                    default:
                        return this.fail(res, error.errorValue().message);
                }

            } else {
                const category = result.value.getValue();

                return this.ok<any>(res,category);
            }

        } catch (err) {
            return this.fail(res, err)
        }
    }
}