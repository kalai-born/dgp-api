import { StoresService } from "../services/StoresService";
import { StoresController } from "./StoresController"
import { storesRepo } from "../repos"

let getStoresService = new StoresService(storesRepo);
let getStoresController = new StoresController(getStoresService)

export {
  getStoresController
}