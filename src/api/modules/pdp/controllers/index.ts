import { PDPService } from "../services/PDPService";
import { PDPController } from "./PDPController";
import { pdpRepo } from "../repos"

let getPDPService = new PDPService(pdpRepo);
let getPDPController = new PDPController(getPDPService)

export {
  getPDPController
}