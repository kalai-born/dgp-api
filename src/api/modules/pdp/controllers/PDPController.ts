
import * as express from 'express'
import { BaseController } from "../../shared/infra/http/models/BaseController";
import { PDPService } from '../services/PDPService';
import { PDPError } from '../errors/PDPError';

export class PDPController extends BaseController {
    private useCase: PDPService;

    constructor(useCase: PDPService) {
        super();
        this.useCase = useCase;
    }

    async executeImpl(req: express.Request, res: express.Response): Promise<any> {

        try {
            const result = await this.useCase.execute(req);

            if (result.isLeft()) {
                const error = result.value;

                switch (error.constructor) {
                    case PDPError.Exception:
                        return this.notFound(res, error.errorValue().message)
                    default:
                        return this.fail(res, error.errorValue().message);
                }

            } else {
                const pdp = result.value.getValue();

                return this.ok<any>(res,pdp);
            }

        } catch (err) {
            return this.fail(res, err)
        }
    }
}