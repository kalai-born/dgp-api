import { ILayoutRepo } from "../../../layout/repos/layout";
import { IPDPLayoutRepo } from "../PDPLayoutRepo"
import { IProductRepo } from "../../../products/repos/products";
import express from "express";
import { getPath } from "../../../../../lib/util";

export class PDPLayout implements IPDPLayoutRepo {
  private layout: ILayoutRepo; product: IProductRepo

  constructor(layout: ILayoutRepo, products: IProductRepo) {
    this.layout = layout;
    this.product = products;
  }

  async getPageLayout(req: express.Request, storeCode: string, field_name: string, pageId: any): Promise<any> {
    let path = getPath(req, 'getPageLayout');
    return this.layout.getPageLayout(path, storeCode, field_name, pageId);
  }

  async getProducts(req: express.Request, storeCode: string, product_skus: Array<string>): Promise<any> {
    let path = getPath(req, 'getProducts');
    return this.product.getProducts(storeCode, path, product_skus);
  }

  async getProductsByURL(req: express.Request, storeCode: string, url_path: string): Promise<any> {
    let path = getPath(req, 'getProductsByURL');
    return this.product.getProductsByURL(storeCode, path, url_path);
  }

}