
import { PDPLayout } from "./implementations/PDPElasticSearch";
import { layoutRepo } from "../../layout/repos";
import { productsRepo } from "../../products/repos"


const pdpRepo = new PDPLayout(layoutRepo,productsRepo);

export { pdpRepo }
