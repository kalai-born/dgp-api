import express from "express";


export interface IPDPLayoutRepo {
  getPageLayout(req:express.Request,storeCode: string, field_name:string,pageId:any): Promise<any>;
  getProducts(req:express.Request,storeCode: string, products: Array<string>): Promise<any>;
  getProductsByURL(req:express.Request,storeCode: string,url_path: string): Promise<any>;
}
