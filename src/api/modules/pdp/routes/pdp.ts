
import express from 'express'
import { getPDPController } from '../controllers';

const pdp = express.Router();
let config: any
let db: any
export default ({ config, db }) => {
  pdp.get('/getPDPLayout',
    (req, res) => getPDPController.execute(req, res))

  return pdp;
}

