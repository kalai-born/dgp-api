import { convertToArray } from '../../../../lib/util'
import { Service } from '../../shared/core/Service'
import { IPDPLayoutRepo } from '../repos/PDPLayoutRepo';
import { Either, Result, left, right } from "../../shared/core/Result"
import { AppError } from "../../shared/core/AppError";
import { Request } from 'express-serve-static-core';
import express from 'express';
const R = require('ramda');
const URL = 'url';
const PAGE_LAYOUT = '/pdp';

type Response = Either<
    AppError.UnexpectedError,
    Result<any>
>
export class PDPService implements Service<Request, Promise<any>> {
    private pdpRepo: IPDPLayoutRepo;

    constructor(pdpRepo: IPDPLayoutRepo) {
        this.pdpRepo = pdpRepo;
    }

    protected getFormatedPDPMiddleContent = (data) => {
        let middleContent = [];
        try {
          middleContent = convertToArray(data);
        } catch (e) { console.log("\nError: PDP Middle content: ", e) }
        return middleContent;
    };

    async execute(request: express.Request): Promise<any> {
        const storeCode = request.query.storeCode.toString();
        

        
        try {
        /* const queryObj = {
            index: config.drupal.homePageComponentsLatest,
            type: '_doc',
            body: {
            query: {
                query_string: {
                fields: ["url"],
                query: "?pdp"
                }
            }
            }
        }; */
        //const batchResults = await doElasticSearch(esClient, queryObj);
        const batchResults = await this.pdpRepo.getPageLayout(request, storeCode, URL, PAGE_LAYOUT);
        let middleContent = this.getFormatedPDPMiddleContent((batchResults[0] && batchResults[0].field_nodejson) ? JSON.parse(batchResults[0].field_nodejson) : {});
        const url_path  = request.query.url_path.toString() || '';
        let productDescription = {};
        if (url_path !== '') {
            /* const productQueryObj = {
            index: config.magento.productIndex,
            type: config.magento.productType,
            body: {
                size: config.magento.maxProduct,
                query: {
                match: { url_path }
                }
            }
            }; */
            //const productRes = await doElasticSearch(esClient, productQueryObj);
            const productRes = await this.pdpRepo.getProductsByURL(request, storeCode, url_path );
            productDescription = productRes && productRes[0] || {};
            let whats_in_the_box_common = productDescription["whats_in_the_box_common"] || '';
            let list = await this.pdpRepo.getProducts(request, storeCode, whats_in_the_box_common.split(",")) || [];
            middleContent.map(item => {
            if (item.value && item.value["field_component_token"] && item.value["field_component_token"] == "what-in-the-box") {
                item.value["productList"] = list;
            }
            return item;
            })
        }
        return right(Result.ok<any>({ middleContent: middleContent }));

        } catch (error) {
            return left(new AppError.UnexpectedError(error));
        }
        
    }
}