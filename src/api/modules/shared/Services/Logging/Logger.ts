const pino = require('pino');
const pino_mq = require('pino-mq');
const R = require('ramda');
import getTransport from './getTransport'

export class Logger {
  private logger: any;
  private config: any;

  constructor(config:any) {
      this.config = config
      this.init(config)
  }

  protected init = (config:any) =>{
        let opts = {};        
        this.logger = pino(opts,getTransport(config))

  }
  public getLogger =() =>{
        return this.logger;
  }
}

