import { matchQuery } from "elastic-builder";

const {getTransport} = require('pino-mq');
const R = require('ramda');
const transports = [
    'pino-mq'
]


export default (config)=>{
    const transport = (transport_method,config) =>{
        switch (transport_method) {
            case "pino-mq":
                return R.path([transport_method,'uri'],config)?getTransport(R.prop("pino-mq",config)):undefined;
            default:
                return undefined;
        }
    }
    let transport_method = R.map((x)=>R.prop(x,config),transports)[0];
    return transport(R.prop('package',transport_method),config);


}