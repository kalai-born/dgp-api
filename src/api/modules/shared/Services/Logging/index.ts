import { Logger } from './Logger'
import config from 'config'

let logger = new Logger(config).getLogger();

export {
    logger,
}