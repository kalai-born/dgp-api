import DataService from './DataService'
import DataService_stores from './DataService_stores';
import config from 'config'

let dataService = new DataService(config);
let dataService_store = new DataService_stores(config)

export{
    dataService,
    dataService_store
}