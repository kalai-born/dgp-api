'use strict';
import { Client } from '@elastic/elasticsearch';
import * as elastic from '../../../../../lib/elastic'
import esb, { boolQuery, BoolQuery, TermsAggregationBase } from 'elastic-builder'; // the builder
const R = require('ramda');
import { logger } from '../../../shared/Services/Logging'

const STORE_VIEWS = 'storeViews';
const ELASTIC_SEARCH = 'elasticsearch';
const DRUPAL = 'drupal';
const INDEX = 'index';
const FIELD_MC_NAME = 'field_machine_name';
const BODY = 'body';


export default class {
    private client: Client;
    private config: any;
    public constructor(config: any) {
        this.config = config;
        this.client = elastic.getClient(this.config)
    }


    protected getEsb(generalQuery: string): any {
        switch (generalQuery) {
            case "boolQuery":
                return {
                    function_name: esb.boolQuery,
                    next_function: "must"
                };
            case "termsQuery":
                return {
                    function_name: esb.termsQuery,
                    next_function: "must"
                };
            case "queryString":
                return { function_name: this.getQueryString }
            case "matchQuery":
                return esb.matchQuery;
            default:
                break;

        }
    }

    protected getQueryString(defaultField: string, queryString: string): any {
        let query = esb.queryStringQuery(queryString).fields([defaultField]);
        return query
    }


    protected getCompoundedQuery(accumalator: Object, field_config: Object) {
        let query = R.prop('query', accumalator);
        let next_function = R.prop('next_function', accumalator);
        let datafields = R.prop('fields', accumalator);
        let originalContext = R.prop('that', accumalator);
        let field = R.keys(field_config)[0];
        let queryFunctionString = R.prop(field, field_config);
        let esbFunction_object = originalContext.getEsb(queryFunctionString);
        let esbFunction = R.prop('function_name', esbFunction_object);
        let esb_next_function = R.prop('next_function', esbFunction_object)
        let derived_query = datafields.get(field) ? query ? query[next_function](esbFunction(field, datafields.get(field))) : esbFunction(field, datafields.get(field)):query
        return R.compose(
            R.assoc('query', derived_query),
            R.assoc('next_function', esb_next_function)
        )(accumalator)
    }

    protected getQuery(queryConfig: Object, fields: Map<string, string>): esb.Query {
        let main_query_name = R.prop('generalQuery', queryConfig)
        let mainQuery = this.getEsb(R.prop('generalQuery', queryConfig))
        let main_function = R.prop('function_name', mainQuery);
        let next_function = R.prop('next_function', mainQuery);
        let that = this;
        let query_fields = R.prop('fields', queryConfig)
        let accumalator = {
            query: main_function ? main_function() : main_function,
            fields: fields,
            that: that,
            next_function: next_function
        }
        let query = R.prop('query', R.reduce(this.getCompoundedQuery, accumalator, query_fields))
        return query;
    }

    async getData(storeCode: string, configPath: Array<string>, fields: Map<string, string>): Promise<any> {
        return new Promise((res, rej) => {
            let queryConfigPath = R.prepend('elasticQueryConfigs', configPath)
            let queryConfig = R.path(queryConfigPath, this.config)

            let query = queryConfig ? esb.requestBodySearch().query(this.getQuery(queryConfig, fields)) : null
            let platform = R.prop('platform', queryConfig)
            let index_type = R.prop('index', queryConfig)
            let index = R.path([STORE_VIEWS, storeCode, ELASTIC_SEARCH, platform, index_type], this.config)

            let search_parm = R.compose(
                R.assoc(BODY, query.toJSON()),
                R.assoc(INDEX, index)
            )({})
            this.client.search(search_parm).then((result) => {
                logger.error("hI test message")
                res(result.body.hits.hits.map(hit => {
                    return Object.assign(hit._source, { _score: hit._score });
                }))
            }).catch((err) => {
                logger.error(err)
                rej(err)
            })
        })
    }
}