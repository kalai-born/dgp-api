'use strict';
import { Client } from '@elastic/elasticsearch';
import * as elastic from '../../../../../lib/elastic'
import esb, { boolQuery, BoolQuery, TermsAggregationBase } from 'elastic-builder'; // the builder
const R = require('ramda');
import {logger} from '../Logging'

const STORE_VIEWS = 'storeViews';
const ELASTIC_SEARCH = 'elasticsearch';
const DRUPAL = 'drupal';
const INDEX = 'index';
const FIELD_MC_NAME = 'field_machine_name';
const BODY = 'body';


export default class  {
    private client: Client;
    private config: any;
    public constructor(config: any) {
        this.config = config;
        this.client = elastic.getClient(this.config)
    }


    protected getEsb(generalQuery: string): any {
        switch (generalQuery) {
            case "boolQuery":
                return esb.boolQuery;
            case "termsQuery":
                return esb.termsQuery;
            case "queryString":
                return this.getQueryString;
            default:
                break;
                         
        }
    }

    protected getQueryString(defaultField:string,queryString:string):any{
        let query = esb.queryStringQuery(queryString).fields([defaultField]);
        return query
    }


    protected getCompoundedQuery(accumalator: Object, field_config: Object) {
        let query = R.prop('query', accumalator);
        let datafields = R.prop('fields', accumalator);
        let originalContext = R.prop('that', accumalator);
        let field = R.keys(field_config)[0];
        let queryFunctionString = R.prop(field, field_config);
        let esbFuntion = originalContext.getEsb(queryFunctionString);
        let derived_query = datafields.get(field) ? query.must(esbFuntion(field, datafields.get(field))) : query
        return R.assoc('query', derived_query, accumalator)
    }

    protected getQuery(queryConfig: Object, fields: Map<string, string>): esb.Query {
        let mainQuery =this.getEsb(R.propOr('','generalQuery', queryConfig))
        let that = this;
        let query_fields = R.prop('fields', queryConfig)
        let accumalator = {
            query: mainQuery(),
            fields: fields,
            that: that
        }
        let query = R.prop('query', R.reduce(this.getCompoundedQuery, accumalator, query_fields))
        return query;
    }

    async getData(storeCode: string, configPath: Array<string>, fields: Map<string, string>): Promise<any> {
        return new Promise((res, rej) => {
            let queryConfigPath = R.prepend('elasticQueryConfigs', configPath)
            let queryConfig = R.path(queryConfigPath, this.config)              
            let query = queryConfig ? esb.requestBodySearch().query(esb.queryStringQuery(fields.get('field_store_code')).fields(['field_store_code'])):null
            let platform = R.prop('platform', queryConfig)
            let index_type = R.prop('index', queryConfig)
            let index = R.path([STORE_VIEWS, storeCode, ELASTIC_SEARCH, platform, index_type], this.config)

            let search_parm = R.compose(
                R.assoc(BODY, query.toJSON()),
                R.assoc('type','_doc'),
                R.assoc(INDEX, index)
            )({})
            this.client.search(search_parm).then((result) => {
                logger.error("hI test message")  
                res(result.body.hits.hits.map(hit => {
                    return Object.assign(hit._source, { _score: hit._score });
                }))
            }).catch((err) => {
                logger.error(err)
                rej(err)
            })
        })
    }
}