
import { PLPLayout } from "./implementations/PLPElasticSearch";
import { layoutRepo } from "../../layout/repos";


const pdpRepo = new PLPLayout(layoutRepo);

export { pdpRepo }
