import express from "express";


export interface IPLPLayoutRepo {
  getPageLayout(req:express.Request,storeCode: string, field_name:string,pageId:any): Promise<any>;
}
