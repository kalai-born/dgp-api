import { ILayoutRepo } from "../../../layout/repos/layout";
import { IPLPLayoutRepo } from "../PLPLayoutRepo"
import { IProductRepo } from "../../../products/repos/products";
import express from "express";
import { getPath } from "../../../../../lib/util";

export class PLPLayout implements IPLPLayoutRepo {
  private layout: ILayoutRepo; 

  constructor(layout: ILayoutRepo) {
    this.layout = layout;
  }

  async getPageLayout(req: express.Request, storeCode: string, field_name: string, pageId: any): Promise<any> {
    let path = getPath(req, 'getPageLayout');
    return this.layout.getPageLayout(path, storeCode, field_name, pageId);
  }
}