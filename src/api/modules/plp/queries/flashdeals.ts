'use strict';
import { Client } from '@elastic/elasticsearch';
import * as elastic from '../../../../lib/elastic'
import esb from 'elastic-builder';
const R = require('ramda');

const STORE_VIEWS = 'storeViews';
const ELASTIC_SEARCH = 'elasticsearch';
const PLATFORM = 'MAGENTO';
const INDEX = 'index';
const FIELD_MC_NAME = 'field_machine_name';
const BODY = 'body';
const DEAL = 'deal'


export default class FlashDeals {
    private client: Client;
    private config: any;
    public constructor(config: any) {
        this.config = config;
        this.client = elastic.getClient(this.config)
    }

    async getDeals(storeCode: string):Promise<any> {
        return new Promise((res, rej) => {
            const requestBody = esb
                .requestBodySearch()
                .query(
                     esb.matchQuery('_type','deal')
                        
                ).size(10)        
            let search_parm = R.compose(
                R.assoc(BODY, requestBody),
                R.assoc(INDEX, R.path([STORE_VIEWS, storeCode, ELASTIC_SEARCH, PLATFORM, DEAL], this.config))
            )({})
            this.client.search(search_parm).then((result) => {
                res(result.body.hits.hits.map(hit => {
                    return Object.assign(hit._source, { _score: hit._score });
                  }))
            }).catch((err) => {
                rej(err)
            })
        })
    }

}