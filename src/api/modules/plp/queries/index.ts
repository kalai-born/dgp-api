import config from 'config'
import FlashDeals from "./flashdeals";

const flashDeals = new FlashDeals(config)

export {
    flashDeals
}