import { convertToArray, getFormattedResults } from '../../../../lib/util'
import { Service } from '../../shared/core/Service'
import { IPLPLayoutRepo } from '../repos/PLPLayoutRepo';
import { Either, Result, left, right } from "../../shared/core/Result"
import { AppError } from "../../shared/core/AppError";
import { Request } from 'express-serve-static-core';
import express from 'express';
const R = require('ramda');
const URL = 'url';
const PAGE_LAYOUT = '/plp';

type Response = Either<
    AppError.UnexpectedError,
    Result<any>
>
export class PLPService implements Service<Request, Promise<any>> {
    private plpRepo: IPLPLayoutRepo;

    constructor(pdpRepo: IPLPLayoutRepo) {
        this.plpRepo = pdpRepo;
    }

    protected getFormatedPDPMiddleContent = (data) => {
        let middleContent = [];
        try {
            middleContent = convertToArray(data);
        } catch (e) { console.log("\nError: PDP Middle content: ", e) }
        return middleContent;
    };

    async execute(request: express.Request): Promise<any> {
        try {
            const storeCode = request.query.storeCode.toString();
            let resData = await this.plpRepo.getPageLayout(request, storeCode, URL, PAGE_LAYOUT);
            let formattedData = getFormattedResults(resData, ['_source', 'field_nodejson'])
            let middleContent = convertToArray(formattedData[0]);
            return right(Result.ok<any>({ middleContent }));
        } catch (error) {
            return left(new AppError.UnexpectedError(error));
        }
    }
}