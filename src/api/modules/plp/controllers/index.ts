import { PLPService } from "../services/PLPService";
import { PLPController } from "./PLPController";
import { pdpRepo } from "../repos"

let getPLPService = new PLPService(pdpRepo);
let getPLPController = new PLPController(getPLPService)

export {
  getPLPController
}