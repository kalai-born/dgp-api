
import * as express from 'express'
import { BaseController } from "../../shared/infra/http/models/BaseController";
import { PLPService } from '../services/PLPService';
import { PLPError } from '../errors/PLPError';

export class PLPController extends BaseController {
    private useCase: PLPService;

    constructor(useCase: PLPService) {
        super();
        this.useCase = useCase;
    }

    async executeImpl(req: express.Request, res: express.Response): Promise<any> {

        try {
            const result = await this.useCase.execute(req);

            if (result.isLeft()) {
                const error = result.value;

                switch (error.constructor) {
                    case PLPError.Exception:
                        return this.notFound(res, error.errorValue().message)
                    default:
                        return this.fail(res, error.errorValue().message);
                }

            } else {
                const pdp = result.value.getValue();

                return this.ok<any>(res,pdp);
            }

        } catch (err) {
            return this.fail(res, err)
        }
    }
}