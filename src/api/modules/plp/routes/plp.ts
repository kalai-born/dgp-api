
import express from 'express'
import { getPLPController } from '../controllers';

const plp = express.Router();
let config: any
let db: any
export default ({ config, db }) => {
  plp.get('/getPLPLayout',
    (req, res) => getPLPController.execute(req, res))

  return plp;
}

