
import { Products } from "./implementations/products";

export interface IProductRepo {
  getProducts (storeCode: string,path:Array<string>,product_sku:Array<string>): Promise<any>;  
  getProductByCategory (storeCode: string,path:Array<string>, category_ids: Array<string>): Promise<any>;
  getLatestProductsByCategory(storeCode:string,path,category_ids:Array<string>): Promise<any>;
  getProductsByURL(storeCode:string,path,url_path: string): Promise<any>;

}
