
import { IProductRepo } from "../products";
import { dataService } from "../../../shared/Services/ElasticSearch"


export class Products implements IProductRepo {

  async getProducts(storeCode: string, path:Array<string>,product_skus: Array<string>): Promise<any> {
    let field_map = new Map();
    field_map.set('sku',product_skus);
    return dataService.getData(storeCode,path,field_map); 
  }
  async getProductByCategory(storeCode: string, path:Array<string>,category_ids: Array<string>): Promise<any> {
     let field_map = new Map()
     field_map.set("category_ids",category_ids);
     return dataService.getData(storeCode,path,field_map) 
  }

  async getLatestProductsByCategory(storeCode: string,path, category_ids: Array<string>): Promise<any> {
     let field_map = new Map()
     field_map.set("category_ids",category_ids);
     field_map.set("new","1")
     return dataService.getData(storeCode,path,field_map) 
  }
  async getProductsByURL(storeCode: string,path, url_path: string): Promise<any> {
    let field_map = new Map()
    field_map.set("url_path",url_path);
    return dataService.getData(storeCode,path,field_map) 
 }
}