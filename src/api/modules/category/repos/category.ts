

export interface ICategoryRepo {
  getCategories (storeCode: string,path:Array<string>,category_ids:Array<string>): Promise<any>;  
}
