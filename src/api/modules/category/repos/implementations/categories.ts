
import { ICategoryRepo } from "../category";
import { dataService } from "../../../shared/Services/ElasticSearch"


export class Category implements ICategoryRepo {

  async getCategories(storeCode: string, path:Array<string>,category_ids: Array<string>): Promise<any> {
    let field_map = new Map();
    field_map.set('id',category_ids);
    return dataService.getData(storeCode,path,field_map); 
  }
}