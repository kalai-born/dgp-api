
import { ServiceError } from "../../shared/core/ServiceError"
import { Result } from "../../shared/core/Result"

export namespace HomePageError {

  export class Exception extends Result<ServiceError> {    
    constructor (email: string) {
      super(false, {
        message: `Unexpected Exception occured`
      } as ServiceError)
    }
  }
}