
import { Service } from '../../shared/core/Service'
import { IHomePageRepo } from '../repos/HomePageRepo';
import { Either, Result, left, right } from "../../shared/core/Result"
import { AppError } from "../../shared/core/AppError";
import { Request } from 'express-serve-static-core';
import express from 'express';

type Response = Either<
  AppError.UnexpectedError,
  Result<any>
>
const HEADER = 'header_menu';
const FOOTER = 'footer_menu';

export class HeaderFooterService implements Service<Request, Promise<any>> {
  private homePageRepo: IHomePageRepo;

  constructor(homePageRepo: IHomePageRepo) {
    this.homePageRepo = homePageRepo;
  }

  protected getformatedHomeHeaderContent(body: any): Object {
    let headerData = {};
    let { mainHeader, primaryHeader, shopallproducts } = body;

    let categoriesList = this.getShopAllProductsData(shopallproducts);
    let mainHeaderData = this.getMainHeadersData(mainHeader);
    let primaryHeaderData = this.getPrimaryHeadersData(primaryHeader);

    headerData = {
      headerMenus: {
        categories: categoriesList,
        ...mainHeaderData,
      },
      ...primaryHeaderData,
    };
    headerData['data'] = mainHeader;

    return headerData;
  };
  protected getPrimaryHeadersData = (primaryHeader) => {
    let primaryHeaderData = {};
    for (var key in primaryHeader) {
      if (!primaryHeader.hasOwnProperty(key)) continue;
      var obj = primaryHeader[key];
      for (var childkey in obj) {
        if (!obj.hasOwnProperty(childkey)) continue;
        var childObj = obj[childkey];
        if (childObj.hasOwnProperty('field_menu_id')) {
          let id = childObj.field_menu_id || '';
          let title = childObj.title || '';
          if (id === 'header-logo') {
            let desktopImg = childObj.field_header_desktop_logo && childObj.field_header_desktop_logo[0] && childObj.field_header_desktop_logo[0].url || '';
            let desktopAlt = childObj.field_header_desktop_logo && childObj.field_header_desktop_logo[0] && childObj.field_header_desktop_logo[0].alt || '';
            let mobileImg = childObj.field_header_mobile_logo && childObj.field_header_mobile_logo[0] && childObj.field_header_mobile_logo[0].url || '';
            let mobileAlt = childObj.field_header_mobile_logo && childObj.field_header_mobile_logo[0] && childObj.field_header_mobile_logo[0].alt || '';
            primaryHeaderData['headerLogo'] = { title, desktopImg, desktopAlt, mobileImg, mobileAlt };

          } else if (id === 'header-search-bar') {
            let description = childObj.description || '';
            primaryHeaderData['searchBar'] = { title, description };

          } else if (id === 'country') {
            primaryHeaderData['country'] = { title }

          } else if (id === 'cart') {
            let img = childObj.field_menu_icons && childObj.field_menu_icons[0] && childObj.field_menu_icons[0].url || '';
            let imgAlt = childObj.field_menu_icons && childObj.field_menu_icons[0] && childObj.field_menu_icons[0].alt || '';
            let url = childObj.link && childObj.link.url || '';
            primaryHeaderData['cart'] = { title, img, imgAlt, url }

          } else if (id === 'user-account') {
            primaryHeaderData['userAccount'] = { title }

          } else if (id === 'signin') {
            let img = childObj.field_menu_icons && childObj.field_menu_icons[0] && childObj.field_menu_icons[0].url || '';
            let imgAlt = childObj.field_menu_icons && childObj.field_menu_icons[0] && childObj.field_menu_icons[0].alt || '';
            let url = childObj.link && childObj.link.url || '';
            primaryHeaderData['signIn'] = { title, img, imgAlt, url }

          } else if (id === 'user-account-signout') {
            let img = childObj.field_menu_icons && childObj.field_menu_icons[0] && childObj.field_menu_icons[0].url || '';
            let imgAlt = childObj.field_menu_icons && childObj.field_menu_icons[0] && childObj.field_menu_icons[0].alt || '';
            let url = childObj.link && childObj.link.url || '';
            primaryHeaderData['signOut'] = { title, img, imgAlt, url }
          }
        } else {
          try {
            let obj = Object.values(childObj);
            for (let index = 0; index < obj.length; index++) {
              const element = obj[index][0];
              let title = element.title;
              let id = element.field_menu_id || '';

              if (id === 'country-list') {
                let img = element.field_menu_icons && element.field_menu_icons[0] && element.field_menu_icons[0].url || '';
                let imgAlt = element.field_menu_icons && element.field_menu_icons[0] && element.field_menu_icons[0].alt || '';
                let url = element.link && element.link.url || '';

                let isAvail = primaryHeaderData['country'] && primaryHeaderData['country'].countryList && primaryHeaderData['country'].countryList.length > 0 || false;
                if (!isAvail) {
                  primaryHeaderData['country'].countryList = []
                }
                primaryHeaderData['country'].countryList.push({ title, img, imgAlt, url })

              } else if (id === 'user-account') {
                let img = element.field_menu_icons && element.field_menu_icons[0] && element.field_menu_icons[0].url || '';
                let imgAlt = element.field_menu_icons && element.field_menu_icons[0] && element.field_menu_icons[0].alt || '';
                let url = element.link && element.link.url || '';

                let isAvail = primaryHeaderData['userAccount'] && primaryHeaderData['userAccount'].userAccountLinks && primaryHeaderData['userAccount'].userAccountLinks.length > 0 || false;
                if (!isAvail) {
                  primaryHeaderData['userAccount'].userAccountLinks = []
                }
                primaryHeaderData['userAccount'].userAccountLinks.push({ title, img, imgAlt, url })
              }
            }
          } catch (e) { }
        }
      }
    }
    return primaryHeaderData;
  };


  protected getMainHeadersData(mainHeader: Object): Object {
    let mainHeaderData = {};
    for (var key in mainHeader) {
      if (!mainHeader.hasOwnProperty(key)) continue;
      var rootMenu = {}
      var linkItems = [];
      var obj = mainHeader[key];
      for (var childkey in obj) {
        if (!obj.hasOwnProperty(childkey)) continue;
        var childObj = obj[childkey];
        if (childObj.hasOwnProperty('field_menu_id')) {
          //console.log("\nchildObj ",childObj);
          let id = childObj.field_menu_id || '';
          let title = childObj.title || '';
          let menuId = childObj.field_menu_id || '';
          if (id === 'sharaf-experience') {
            mainHeaderData['sharafExperiences'] = { title, menuId };
            if (childObj.field_additional_card && childObj.field_additional_card[0]) {
              mainHeaderData['sharafExperiences'].experiences = this.getSharafExperience(childObj.field_additional_card[0]);
            }
          } else if (id === 'brands') {
            mainHeaderData['brands'] = { title, menuId }
            if (childObj.field_additional_card && childObj.field_additional_card[0]) {
              mainHeaderData['brands'].exclusiveBrandsTitle = childObj.field_additional_card[0].field_title;
              mainHeaderData['brands'].exclusiveBrandDetails = this.getExclusiveBrand(childObj.field_additional_card[0]);
            }
          } else if (id === 'top-deals') {
            mainHeaderData['topDeals'] = { title, menuId, menuList: [], menuListWithSubMenu: [] }
          } else if (id === 'latest-arrivals') {
            mainHeaderData['latestArrivals'] = { title, menuId, menuList: [], menuListWithSubMenu: [] }
          } else if (id === 'best-seller') {
            mainHeaderData['bestSellers'] = { title, menuId, menuList: [], menuListWithSubMenu: [] }
          }
        } else {
          try {
            let obj = Object.keys(childObj);

            obj.map((item, index) => {
              const element = childObj[item][0];
              let id = element.field_menu_id || '';

              if (id === 'see-all-link-brands') {
                mainHeaderData['brands'].viewAll = element.title;
                mainHeaderData['brands'].viewAllRedirectUrl = element.link && element.link.url;
                mainHeaderData['brands'].target = element.field_redirection_link_type || '_self';

              } else if (id === 'see-all-link-sharaf') {
                mainHeaderData['sharafExperiences'].linkText = element.title;
                mainHeaderData['sharafExperiences'].linkUrl = element.link && element.link.url;
                mainHeaderData['sharafExperiences'].target = element.field_redirection_link_type || '_self';

              } else if (id === 'see-all-link-top-deals') {
                mainHeaderData['topDeals'].seeAllText = element.title;
                mainHeaderData['topDeals'].seeAllUrl = element.link && element.link.url;
                mainHeaderData['topDeals'].target = element.field_redirection_link_type || '_self';

              } else if (id === 'see-all-link-best-seller') {
                mainHeaderData['bestSellers'].seeAllText = element.title;
                mainHeaderData['bestSellers'].seeAllUrl = element.link && element.link.url;
                mainHeaderData['bestSellers'].target = element.field_redirection_link_type || '_self';

              } else if (id === 'see-all-link-latest-arrivals') {
                mainHeaderData['latestArrivals'].seeAllText = element.title;
                mainHeaderData['latestArrivals'].seeAllUrl = element.link && element.link.url;
                mainHeaderData['latestArrivals'].target = element.field_redirection_link_type || '_self';

              } else if (id === 'additional-cards') {
                mainHeaderData['topDeals'].skus = element.field_additional_card && element.field_additional_card[0];

              } else if (id === 'additional-card-best-seller') {
                mainHeaderData['bestSellers'].skus = element.field_additional_card && element.field_additional_card[0];

              } else if (id === 'additional-card-latest-arrival') {
                mainHeaderData['latestArrivals'].skus = element.field_additional_card && element.field_additional_card[0];

              } else if (id === 'top-deals') {
                let title = element.title || '';
                let url = element && element.link && element.link.url || '';
                let target = element.field_redirection_link_type || '_self';
                let subMenus = this.getSubMenus(childObj[item]['linkItems']);
                let listObj = { title, url, target };
                if (subMenus.length > 0) {
                  listObj['subMenus'] = subMenus;
                  mainHeaderData['topDeals'].menuListWithSubMenu.push(listObj);
                } else {
                  mainHeaderData['topDeals'].menuList.push(listObj);
                }
              } else if (id === 'latest-arrivals') {

                let title = element.title || '';
                let url = element && element.link && element.link.url || '';
                let target = element.field_redirection_link_type || '_self';
                let subMenus = this.getSubMenus(childObj[item]['linkItems']);
                let listObj = { title, url, target };
                if (!(mainHeaderData['latestArrivals'] && mainHeaderData['latestArrivals'].hasOwnProperty('menuList'))) {
                  mainHeaderData['latestArrivals'].menuList = [];
                }
                if (!(mainHeaderData['latestArrivals'] && mainHeaderData['latestArrivals'].hasOwnProperty('menuListWithSubMenu'))) {
                  mainHeaderData['latestArrivals'].menuListWithSubMenu = [];
                }
                if (subMenus.length > 0) {
                  listObj['subMenus'] = subMenus;
                  mainHeaderData['latestArrivals'].menuListWithSubMenu.push(listObj);
                } else {
                  mainHeaderData['latestArrivals'].menuList.push(listObj);
                }
              } else if (id === 'best-seller') {

                let title = element.title || '';
                let url = element && element.link && element.link.url || '';
                let target = element.field_redirection_link_type || '_self';
                let subMenus = this.getSubMenus(childObj[item]['linkItems']);
                let listObj = { title, url, target };
                if (!(mainHeaderData['bestSellers'] && mainHeaderData['bestSellers'].hasOwnProperty('menuList'))) {
                  mainHeaderData['bestSellers'].menuList = [];
                }
                if (!(mainHeaderData['bestSellers'] && mainHeaderData['bestSellers'].hasOwnProperty('menuListWithSubMenu'))) {
                  mainHeaderData['bestSellers'].menuListWithSubMenu = [];
                }
                if (subMenus.length > 0) {
                  listObj['subMenus'] = subMenus;
                  mainHeaderData['bestSellers'].menuListWithSubMenu.push(listObj);
                } else {
                  mainHeaderData['bestSellers'].menuList.push(listObj);
                }
              }
            })

          } catch (e) { }
        }
      }

    }

    return mainHeaderData;
  };
  protected getSharafExperience = (experienceList: any) => {
    let sharafExperience = [];
    let { field_experience_card = [] } = experienceList;
    sharafExperience = field_experience_card.map((exp, i) => {
      let imgPath = exp.field_experience_card_image && exp.field_experience_card_image[0] && exp.field_experience_card_image[0].url || '';
      let imgAlt = exp.field_experience_card_image && exp.field_experience_card_image[0] && exp.field_experience_card_image[0].alt || '';
      let url = exp.field_link && exp.field_link.url || '';
      let target = exp.field_redirection_link_type || '_self';

      let expObj = {
        image: imgPath,
        altText: imgAlt,
        url,
        target,
        title: exp.field_title,
        description: exp.field_description,
      };
      return expObj;
    });
    return sharafExperience;
  };
  protected getShopAllData(data: Object): Object {
    let shopAllTitle = '', shopAllList = {};
    let menuId = '';
    for (const key in data) {
      if (!data.hasOwnProperty(key)) continue;
      let obj = data[key];
      shopAllTitle = obj['0'] && obj['0'].title || '';
      menuId = obj['0'] && obj['0'].field_menu_id || '';
      shopAllList = obj.linkItems || {};
    }
    return { shopAllTitle, menuId, shopAllList }
  };
  protected getShopAllProductsData(shopAllData: Object): Object {
    let shopAllProductsList = [];

    let formattedShopallData = this.getShopAllData(shopAllData);
    let shopAllTitle = formattedShopallData['shopAllTitle']
    let menuId = formattedShopallData['menuId'];
    let shopAllList = formattedShopallData['shopAllList'];

    for (var key in shopAllList) {
      if (!shopAllList.hasOwnProperty(key)) continue;
      var rootMenu = {}
      var linkItems = [];
      var obj = shopAllList[key];
      let seeAll = '';
      let featuredBrand = '';
      let experienceCard = '';
      let flashDealsCard = '';
      for (var childkey in obj) {

        if (!obj.hasOwnProperty(childkey)) continue;
        var childObj = obj[childkey];
        if (childObj['menu_name'] === 'shop-all-products') {
          rootMenu['name'] = childObj.title;
          rootMenu['id'] = childObj.title;
          rootMenu['url'] = childObj.link && childObj.link.url;
          rootMenu['target'] = childObj.field_redirection_link_type || '_self';
        } else {
          for (var subChildKey in childObj) {
            if (!childObj.hasOwnProperty(subChildKey)) continue;
            var subChildObj = childObj[subChildKey];

            let subMenu = {}
            let subLinkItems = [];
            for (var innerSubChildkey in subChildObj) {

              if (!subChildObj.hasOwnProperty(innerSubChildkey)) continue;
              var innerSubChildObj = subChildObj[innerSubChildkey];

              if (innerSubChildObj['menu_name'] === 'shop-all-products') {
                if (innerSubChildObj.field_menu_id === 'see-all-link') {
                  seeAll = innerSubChildObj.title;
                } else if (innerSubChildObj.field_menu_id === 'additional-cards') {
                  if (innerSubChildObj.field_additional_card && innerSubChildObj.field_additional_card.length > 0) {
                    innerSubChildObj.field_additional_card.map((cardItem, i) => {
                      if (cardItem.hasOwnProperty('field_brand_card')) {
                        if (cardItem && cardItem.field_brand_card && cardItem.field_brand_card.length > 0) {
                          cardItem.field_brand_card.map((brandItem, i) => {
                            brandItem.target = brandItem.field_redirection_link_type || '_self';
                            return brandItem;
                          });
                        }
                        featuredBrand = cardItem;
                      } else if (cardItem.hasOwnProperty('field_experience_card')) {
                        if (cardItem && cardItem.field_experience_card && cardItem.field_experience_card.length > 0) {
                          cardItem.field_experience_card.map((expItem, i) => {
                            expItem.target = expItem.field_redirection_link_type || '_self';
                            return expItem;
                          });
                        }
                        experienceCard = cardItem;
                      } else {
                        cardItem.target = cardItem.field_redirection_link_type || '_self';
                        flashDealsCard = cardItem;
                      }
                    });
                  }
                } else {
                  subMenu['name'] = innerSubChildObj.title;
                  subMenu['id'] = innerSubChildObj.title;
                  subMenu['url'] = innerSubChildObj.link && innerSubChildObj.link.url;
                  subMenu['target'] = innerSubChildObj.field_redirection_link_type || '_self';
                  subMenu['parent_id'] = this.convertToId(rootMenu['name']);
                }
              } else {
                for (var k in innerSubChildObj) {
                  if (!innerSubChildObj.hasOwnProperty(k)) continue;
                  let Obj = innerSubChildObj[k];
                  let innerSubMenu = {};
                  for (let innerKey in Obj) {
                    if (!Obj.hasOwnProperty(innerKey)) continue;

                    let innerObj = Obj[innerKey];
                    if (innerObj['menu_name'] === 'shop-all-products') {
                      innerSubMenu['name'] = innerObj.title;
                      innerSubMenu['id'] = innerObj.title;
                      innerSubMenu['url'] = innerObj.link && innerObj.link.url;
                      innerSubMenu['target'] = innerObj.field_redirection_link_type || '_self';
                      subLinkItems.push(innerSubMenu);
                    }
                  }
                }
              }
            }
            if (subLinkItems.length > 0) {
              subMenu['subcategories'] = subLinkItems;
            }
            if (seeAll == '') {
              linkItems.push(subMenu);
            }
          }
        }
      }

      if (featuredBrand !== '') {
        rootMenu['featuredBrand'] = featuredBrand;
      }
      if (experienceCard !== '') {
        rootMenu['experienceCard'] = experienceCard;
      }
      if (flashDealsCard !== '') {
        rootMenu['flashDealsCard'] = flashDealsCard;
      }

      rootMenu["subcategories"] = linkItems;
      if (seeAll !== '') {
        rootMenu['seeAll'] = seeAll;
      }

      shopAllProductsList.push(rootMenu);
    }

    return {
      categoryItems: shopAllProductsList,
      title: shopAllTitle,
      menuId,
    };
  };

  protected convertToId = (idData: any) => {
    let data = idData;
    data = idData.toLowerCase().replace(/ /g, '-');
    return data;
  }

  protected getExclusiveBrand = (brandList: any) => {
    let exclusiveBrand = [];
    let { field_brand_card = [] } = brandList;
    exclusiveBrand = field_brand_card.map((exp, i) => {
      let imgPath = exp.field_brand_image && exp.field_brand_image[0] && exp.field_brand_image[0].url || '';
      let imgAlt = exp.field_brand_image && exp.field_brand_image[0] && exp.field_brand_image[0].alt || '';
      let title = exp.field_link && exp.field_link.title || '';
      let url = exp.field_link && exp.field_link.url || '';
      let target = exp.field_redirection_link_type || '_self';
      let expObj = {
        brandLogo: imgPath,
        altText: imgAlt,
        redirectUrl: url,
        target,
        name: title,
      };
      return expObj;
    });
    return exclusiveBrand;
  };

  protected getSubMenus = (listItems: any) => {
    let subMenusItems = [];
    try {
      let subObj = Object.values(listItems || {});
      for (let i = 0; i < subObj.length; i++) {
        const subElement = subObj[i][0];
        let menuObj = {
          title: subElement.title,
          url: subElement.link && subElement.link.url,
          target: subElement.field_redirection_link_type || '_self'
        };
        subMenusItems.push(menuObj);
      }
    } catch (e) { console.log("\nE: ", e) }
    return subMenusItems;
  };

  protected getMainFooterData = (mainFooterData: any): Object => {
    let mainFooterMenuList = [];
    try {
      for (var key in mainFooterData) {
        if (!mainFooterData.hasOwnProperty(key)) continue;
        var rootMenu = {}
        var linkItems = [];
        var obj = mainFooterData[key];
        for (var childkey in obj) {
          let groupMember = [], socialLinks = [];
          if (!obj.hasOwnProperty(childkey)) continue;
          var childObj = obj[childkey];
          if (childObj['field_menu_id']) {
            rootMenu['linkTitle'] = childObj.title;
            if (childObj.field_menu_id === 'payment-method') {
              rootMenu['paymentMethod'] = [];
            }
          } else {
            for (var a in childObj) {
              if (!childObj.hasOwnProperty(a)) continue;
              const obj = childObj[a] && childObj[a][0] || {};
              const field_menu_id = obj && obj.field_menu_id || '';

              let subchildObj = {};

              if (field_menu_id === 'payment-method') {
                const { url, alt } = obj.field_footer_image_link && obj.field_footer_image_link[0] || {};
                subchildObj = {
                  img: url || '',
                  altText: alt || '',
                  redirectUrl: obj.title || '',
                  target: obj.field_redirection_link_type || '',
                };
              } else if (field_menu_id === 'member-group') {
                const { url, alt } = obj.field_footer_image_link && obj.field_footer_image_link[0] || {};
                subchildObj = {
                  img: url || '',
                  altText: alt || '',
                  redirectUrl: obj.title || '',
                  target: obj.field_redirection_link_type || ''
                };
                if (!rootMenu.hasOwnProperty('groupMember')) {
                  rootMenu['groupMember'] = [];
                }
                rootMenu['groupMember'].push(subchildObj);
              } else if (field_menu_id === 'social-links') {
                const { url, alt } = obj.field_footer_image_link && obj.field_footer_image_link[0] || {};
                subchildObj = {
                  img: url || '',
                  altText: alt || '',
                  redirect: obj.title || '',
                  target: obj.field_redirection_link_type || ''
                };
                if (!rootMenu.hasOwnProperty('socialLinks')) {
                  rootMenu['socialLinks'] = [];
                }
                rootMenu['socialLinks'].push(subchildObj);
              } else if (field_menu_id === 'email') {
                rootMenu['emailLabel'] = obj.title || '';
                try {
                  let messageData = obj.field_menu_description && childObj[a][0].field_menu_description.split('|') || '';
                  let messageObj = {};
                  messageData.map((msg, i) => {
                    let item = msg.split('--');
                    messageObj = {
                      ...messageObj,
                      [item[0]]: item[1],
                    }
                  });
                  rootMenu['message'] = messageObj;
                } catch (e) { }

              } else {
                subchildObj = {
                  redirectText: obj.title || '',
                  redirectUrl: obj.redirectUrl || '',
                  target: obj.field_redirection_link_type || ''
                };
              }

              linkItems.push(subchildObj);
            }
          }
        }
        if (rootMenu.hasOwnProperty('paymentMethod')) {
          rootMenu['paymentMethod'] = linkItems;
        } else if (!rootMenu.hasOwnProperty('groupMember')) {
          rootMenu['linkItems'] = linkItems
        }

        mainFooterMenuList.push(rootMenu);
      }
    } catch (e) { console.log("\nError - Main footer: ", e); }

    return mainFooterMenuList;
  };

  protected getPrimaryFooterData = (primaryFooterData: any): Object => {
    let primaryFooter = {
      helpLinks: []
    };

    for (var key in primaryFooterData) {
      if (!primaryFooterData.hasOwnProperty(key)) continue;
      var obj = primaryFooterData[key] && primaryFooterData[key][0] || {};

      if (obj.field_menu_id === 'need-help') {
        primaryFooter['needHelptext'] = obj.title;
        primaryFooter['reachOut'] = obj.field_primary_footer_description;
      } else {
        const { url, alt } = obj.field_primary_footer_image && obj.field_primary_footer_image[0] || {};
        const helpLinkObj = {
          imgIcon: url,
          imgAlt: alt,
          title: obj.title,
          id: obj.field_menu_id,
          titleDesc: obj.field_primary_footer_description,
          redirect: obj.link && obj.link.url || '',
          target: obj.field_redirection_link_type || '_self'
        };
        primaryFooter.helpLinks.push(helpLinkObj);
      }
    }

    return primaryFooter;
  };

  protected getSecondaryFooterData(secondaryFooterData: any): Object {
    let secondaryFooter = {
      makeMoneyTxt: []
    };
    for (var key in secondaryFooterData) {
      if (!secondaryFooterData.hasOwnProperty(key)) continue;
      var obj = secondaryFooterData[key] && secondaryFooterData[key][0] || {};

      if (obj.field_menu_id === 'make-money') {
        secondaryFooter['makeMoneyText'] = obj.title;
      } else {
        const makeMoneyTxt = {
          title: obj.title,
          shortdesc: obj.field_secondary_footer_descripti,
          target: obj.field_redirection_link_type,
        };
        secondaryFooter.makeMoneyTxt.push(makeMoneyTxt);
      }
    }

    return secondaryFooter;
  };

  protected getCopyrightsData(copyrightsFooterData: any): Object {
    let copyrightsFooter = {
      links: []
    };
    for (var key in copyrightsFooterData) {
      if (!copyrightsFooterData.hasOwnProperty(key)) continue;
      var obj = copyrightsFooterData[key] && copyrightsFooterData[key][0] || {};

      if (obj.field_menu_id === 'footer-logo') {
        const { url, alt } = obj.field_copyrights_footer_image && obj.field_copyrights_footer_image[0] || {};
        copyrightsFooter['footerLogoImg'] = url;
        copyrightsFooter['footerLogoImgAlt'] = alt;
      } else if (obj.field_menu_id === 'copy-write') {
        copyrightsFooter['copyrightText'] = obj.field_copyrights_footer_descript;
      } else {
        const linkObj = {
          title: obj.title,
          redirectUrl: obj.link && obj.link.url || '',
          target: obj.field_redirection_link_type || '_self'
        };
        copyrightsFooter.links.push(linkObj);
      }
    }

    return copyrightsFooter;
  };


  protected getformatedHomeFooterContent = (data: any) => {

    let { mainFooter = {}, primaryFooter = {}, secondaryFooter = {}, copyrights = {} } = data;

    let mainFooterData = this.getMainFooterData(mainFooter);
    let primaryFooterData = this.getPrimaryFooterData(primaryFooter);
    let secondaryFooterData = this.getSecondaryFooterData(secondaryFooter);
    let copyrightsData = this.getCopyrightsData(copyrights);
    let footerData = {};
    footerData['data'] = data;
    footerData["needHelp"] = primaryFooterData;
    footerData["makeMoney"] = secondaryFooterData;
    footerData["footerLinks"] = mainFooterData;
    footerData["copyrights"] = copyrightsData;

    return footerData;
  };
  async execute(request: express.Request): Promise<any> {
    let header_footer: any;
    const storeCode = request['query'].storeCode.toString();
    try {
      let header = HEADER + '_' + storeCode;
      let footer = FOOTER + '_' + storeCode;
      let header_data = await this.homePageRepo.getPageLayout(request,storeCode,'field_machine_name', [header]);
      let footer_data = await this.homePageRepo.getPageLayout(request,storeCode,'field_machine_name', [footer]);
      let resData = { header: {}, footer: {} }
      resData.header = this.getformatedHomeHeaderContent((header_data && header_data[0].body) ? JSON.parse(header_data[0].body) : {});
      resData.footer = this.getformatedHomeFooterContent((footer_data && footer_data[0].body) ? JSON.parse(footer_data[0].body) : {});
      try {

        if (resData.header && resData.header['headerMenus'] && (resData.header['headerMenus'].topDeals || resData.header['headerMenus'].latestArrivals || resData.header['headerMenus'].bestSellers)) {
          try {
            const topDealsSkus = resData.header['headerMenus'].topDeals.skus || {};
            const latestArrivalSkus = resData.header['headerMenus'].latestArrivals.skus || {};
            const bestSellerSkus = resData.header['headerMenus'].bestSellers.skus || {};
            const skuListTopDeals = topDealsSkus.field_product_sku && topDealsSkus.field_product_sku.map((skuObj, i) => {
              return skuObj.value;
            });
            const skuListLatestArrival = latestArrivalSkus.field_product_sku && latestArrivalSkus.field_product_sku.map((skuObj, i) => {
              return skuObj.value;
            });
            const skuListBestSeller = bestSellerSkus.field_product_sku && bestSellerSkus.field_product_sku.map((skuObj, i) => {
              return skuObj.value;
            });

            resData.header['headerMenus'].topDeals.productList = await this.homePageRepo.getProducts(request,storeCode,skuListTopDeals);
            resData.header['headerMenus'].latestArrivals.productList = await this.homePageRepo.getProducts(request,storeCode,skuListLatestArrival);
            resData.header['headerMenus'].bestSellers.productList = await this.homePageRepo.getProducts(request,storeCode,skuListBestSeller);
          } catch (e) {
            console.log("\nProduct list for header menus. ", e);
          }

        }
        return right(Result.ok<any>(resData));
      } catch (err) {
        return left(new AppError.UnexpectedError(err));
      }
    } catch (err) {
      return left(new AppError.UnexpectedError(err));
    }

  }
}