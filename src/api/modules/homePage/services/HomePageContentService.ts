
import { IHomePageRepo } from '../repos/HomePageRepo';
import { Either, Result, left, right } from "../../shared/core/Result"
import { AppError } from "../../shared/core/AppError";
import { Request } from 'express-serve-static-core';
import { convertToArray, filterProductAttributtes } from '../../../extensions/common/index'
import * as express from 'express';
import { Service } from '../../shared/core/Service';
const R = require('ramda');

type Response = Either<
  AppError.UnexpectedError,
  Result<any>
>

const skipAttributeList = [
  'id',
  'body',
  'changed',
  'created',
  'default_langcode',
  'langcode',
  'nid',
  'path',
  'promote',
  'revision_default',
  'revision_log',
  'revision_timestamp',
  'revision_translation_affected',
  'revision_uid',
  'status',
  'sticky',
  'title',
  'type',
  'uid',
  'uuid',
  'vid',
];


export class HomePageContentService implements Service<Request, Promise<any>> {
  private homePageRepo: IHomePageRepo;

  constructor(homePageRepo: IHomePageRepo) {
    this.homePageRepo = homePageRepo;
  }

  protected getFormatedHomeMiddleContent = (data: Object): Object => {
    let middleData = [];
    if (data) {
      try {
        middleData = convertToArray(data, skipAttributeList);
        //middleData.push(data);
      } catch (e) {
        console.log("Error: ", e);
      }
    }

    return middleData;
  }

  protected getTopOfferDealList = (dealList: Array<Object>, maxDealCount: number) => {
    dealList.sort((a, b) => b['discount_percentage'] - a['discount_percentage']);
    return dealList.slice(0, maxDealCount)
  };

  async execute(req: express.Request): Promise<any> {
    try {
      let responseData = {};
      let path: Array<string> = [];
      const storeCode = req.query && req.query['storeCode'].toString() || '';
      const device = req.query && req.query.device || '';
      if (device === 'mobile') {
        let mobileProducts = await this.homePageRepo.getProductsByCategory(req, storeCode, ['39', '43']);
        let mobilePage = await this.homePageRepo.getPageLayout(req, storeCode, 'url', ['?mobile-home-page']);
        const mobileData = (mobilePage && mobilePage[0] && mobilePage[0].field_nodejson) ? JSON.parse(mobilePage[0].field_nodejson) : {};
        responseData = mobileData;
        //responseData.allData = responseMobile;
        for (const key in responseData) {
          if (Object.hasOwnProperty.call(responseData, key)) {
            try {
              const keyName = key.split('--')[0];
              const elementObj = responseData[key] || {};
              if (keyName === 'mobile_dynamic_product_widgets') {
                const widgetCategory = elementObj.field_widget_category || '';
                if (widgetCategory === 'deal_product_timer_swiper_slider') {
                  // Flash Deals
                  let dealProductResult = await this.homePageRepo.getFlashDeals(req, storeCode);
                  try {
                    let dealList = dealProductResult || [];
                    const skuList = dealList.map((deal, i) => {
                      return deal.product_sku
                    });
                    const productResult = await this.homePageRepo.getProducts(req, storeCode, skuList);
                    responseData[key].productList = productResult || [];
                  } catch (e) {
                    console.log("\nError(mobile): while getting product info using deals sku.", e);
                  }
                } else if (widgetCategory === 'menu_product_swiper_slider') {
                  // New Arrivals
                  let categoryArray = responseData[key].field_category_lists || [];
                  for (let i = 0; i < categoryArray.length; i++) {
                    const element = categoryArray[i];
                    let catArray = [];
                    let catId = element.field_category && element.field_category.field_category_id || '';
                    catArray.push(catId);
                    const productResult = await this.homePageRepo.getLatestProductsByCategory(req, storeCode, catArray)
                    responseData[key].field_category_lists[i].productList = productResult || [];
                  }
                } else if (widgetCategory === 'product_swiper_slider') {
                  // Recommended for you  OR  Browsing History
                  responseData[key].productList = mobileProducts;
                }
              }
            } catch (e) {
              console.log("\nError(mobile): while adding product list with home page data.", e);
            }
          }
        }
      } else {
        try {
          let deskTopProducts = await this.homePageRepo.getProductsByCategory(req, storeCode, ['39', '43']);
          let deskTopPage = await this.homePageRepo.getPageLayout(req, storeCode, 'url', '/home-page');
          responseData = { middleContent: [] };

          responseData['middleContent'] = this.getFormatedHomeMiddleContent((deskTopPage && deskTopPage[0] && deskTopPage[0].field_nodejson) ? JSON.parse(deskTopPage[0].field_nodejson) : {});
          responseData['middleContent'].map((item, i) => {
            if (item.name === 'dynamic_product_widgets' && item.field_component_token !== 'new-arrivals' && item.field_component_token !== 'flash-deals') {
              //const filteredAttrData = filterProductAttributtes(batchResults[1], includeProductAttr);
              item.value.productList = deskTopProducts || [];
            }
            return item;
          });

          if (responseData['header'] && responseData['header'].headerMenus && responseData['header'].headerMenus.topDeals) {
            let { topDealsSkus = {} } = responseData['header'].headerMenus.topDeals;
            let skuList = topDealsSkus.field_product_sku && topDealsSkus.field_product_sku.map((skuObj, i) => {
              return skuObj.value;
            });

            responseData['header'].headerMenus.topDeals.productList = [];
            const topDealProductResult = await this.homePageRepo.getProducts(req, storeCode, skuList)
            responseData['header'].headerMenus.topDeals.productList = topDealProductResult || [];
          }

          let middleContentArr = responseData['middleContent'];
          for (let indexVal = 0; indexVal < middleContentArr.length; indexVal++) {
            const elementMiddleContent = middleContentArr[indexVal];
            if (elementMiddleContent.name === 'dynamic_product_widgets' && elementMiddleContent.value.field_component_token === 'new-arrivals') {
              let categoryArray = elementMiddleContent.value.field_category_list || [];
              try {
                for (let i = 0; i < categoryArray.length; i++) {
                  const element = categoryArray[i];
                  let catArray = [];
                  let catId = element.field_category_name && element.field_category_name.field_category_id || '';
                  catArray.push(catId);
                  const productResult = await this.homePageRepo.getLatestProductsByCategory(req, storeCode, catArray)
                  elementMiddleContent.value.field_category_list[i].productList = productResult || [];
                }
              } catch (e) { console.log("\nError: Fetching product list for", e) }
            }
            else if (elementMiddleContent.name === 'dynamic_product_widgets' && elementMiddleContent.value.field_component_token === 'flash-deals') {
              const dealProductResult = await this.homePageRepo.getFlashDeals(req, storeCode)
              const maxDealsToFilter = 10; // Need to take this input from Drupal config.
              const topOfferDeal = this.getTopOfferDealList(dealProductResult, maxDealsToFilter);
              elementMiddleContent.value.productDealsInfo = topOfferDeal || [];
              try {
                let dealList = dealProductResult || [];
                let skuList = dealList.map((deal, i) => {
                  return deal.product_sku
                });
                const productResult = await this.homePageRepo.getProducts(req, storeCode, skuList)
                elementMiddleContent.value.productList = productResult || [];
              } catch (e) {
                console.log("\nError: while getting product info using deals sku.", e);
              }
            }
          }
        } catch (error) {
          return left(new AppError.UnexpectedError(error));
        }

      }

      return right(Result.ok<any>(responseData));
    } catch (error) {
      return left(new AppError.UnexpectedError(error));
    }
  }
}