
import { Service } from '../../shared/core/Service'
import { IHomePageRepo } from '../repos/HomePageRepo';
import { Either, Result, left, right } from "../../shared/core/Result"
import { AppError } from "../../shared/core/AppError";
import { Request } from 'express-serve-static-core';
import express from 'express';
const R = require('ramda');

type Response = Either<
    AppError.UnexpectedError,
    Result<any>
>
export class FlashDealsService implements Service<Request, Promise<any>> {
    private homePageRepo: IHomePageRepo;

    constructor(homePageRepo: IHomePageRepo) {
        this.homePageRepo = homePageRepo;
    }

    async execute(request: express.Request): Promise<any> {
        const storeCode = request.query.storeCode.toString();
        try {
            let deals = await this.homePageRepo.getFlashDeals(request,storeCode);
            let productList = R.map((x) => R.prop('product_sku', x), deals)
            let products = await this.homePageRepo.getProducts(request,storeCode,productList)
            let dealsObject = R.compose(
                R.assoc('productList',deals),
                R.assoc('productDealsInfo',products)
            )({})
            return right(Result.ok<any>(dealsObject));    
        } catch (err) {
            return left(new AppError.UnexpectedError(err));
        }
    }
}