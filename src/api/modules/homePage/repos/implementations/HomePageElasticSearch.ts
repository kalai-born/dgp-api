
import { IHomePageRepo } from "../HomePageRepo";
import { ILayoutRepo } from "../../../layout/repos/layout";
import { flashDeals } from "../../queries";
import { IProductRepo } from "../../../products/repos/products";
import express from "express";
import { dataService } from "../../../shared/Services/ElasticSearch"
import { getPath } from "../../../../../lib/util";
import { data } from "src/magento2-rest-client/lib/log";

export class HomePageLayout implements IHomePageRepo {
  private layout: ILayoutRepo; product: IProductRepo

  constructor(layout: ILayoutRepo, products: IProductRepo) {
    this.layout = layout;
    this.product = products;
  }

  async getPageLayout(req: express.Request, storeCode: string, field_name: string, pageId: any): Promise<any> {
    let path = getPath(req, 'getPageLayout');
    return this.layout.getPageLayout(path, storeCode, field_name, pageId);
  }

  async getFlashDeals(req: express.Request, storeCode: string): Promise<any> {
    let path = getPath(req, 'getFlashDeals');
    let field_map = new Map();
    field_map.set("_type", "deal")
    return dataService.getData(storeCode, path, field_map)
  }

  async getProducts(req: express.Request, storeCode: string, product_skus: Array<string>): Promise<any> {
    let path = getPath(req, 'getProducts');
    return this.product.getProducts(storeCode, path, product_skus);
  }

  async getProductsByCategory(req: express.Request, storeCode: string, category_ids: Array<string>): Promise<any> {
    let path = getPath(req, 'getProductsByCategory');
    return this.product.getProductByCategory(storeCode, path, category_ids);
  }
  async getLatestProductsByCategory(req: express.Request, storeCode: string, category_ids: Array<string>): Promise<any> {
    let path = getPath(req, 'getLatestProductsByCategory');
    return this.product.getLatestProductsByCategory(storeCode,path, category_ids);
  }
}