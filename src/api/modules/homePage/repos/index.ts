
import { HomePageLayout } from "./implementations/HomePageElasticSearch";
import { layoutRepo } from "../../layout/repos";
import { productsRepo } from "../../products/repos"


const homePageRepo = new HomePageLayout(layoutRepo,productsRepo);

export { homePageRepo }
