import express from "express";


export interface IHomePageRepo {
  getPageLayout(req:express.Request,storeCode: string, field_name:string,pageId:any): Promise<any>;
  getFlashDeals(req:express.Request,storeCode: string): Promise<any>;
  getProducts(req:express.Request,storeCode: string, products: Array<string>): Promise<any>;
  getProductsByCategory(req:express.Request,storeCode: string,category_ids: Array<string>): Promise<any>
  getLatestProductsByCategory(req:express.Request,storeCode: string, category_ids: Array<string>): Promise<any>
}
