
import express from 'express'
import { getHeaderFooterController, getFlashDealsController, getHomePageContentController } from '../controllers';

const homePage = express.Router();
let config: any
let db: any
export default ({ config, db }) => {
  homePage.get('/headerFooterData',
    (req, res) => getHeaderFooterController.execute(req, res))

  homePage.get('/getFlashdeals',
    (req, res) => getFlashDealsController.execute(req, res))

  homePage.get('/homePageData',
    (req, res) => getHomePageContentController.execute(req, res)
  )
  return homePage;
}

