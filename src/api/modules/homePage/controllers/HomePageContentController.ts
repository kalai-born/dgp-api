
import * as express from 'express'
import { BaseController } from "../../shared/infra/http/models/BaseController";
import { HomePageContentService } from '../services/HomePageContentService';
import { HomePageError } from '../errors/HomePageError';

export class HomePageContentController extends BaseController {
    private useCase: HomePageContentService;

    constructor(useCase: HomePageContentService) {
        super();
        this.useCase = useCase;
    }

    async executeImpl(req: express.Request, res: express.Response): Promise<any> {

        try {
            const result = await this.useCase.execute(req);

            if (result.isLeft()) {
                const error = result.value;

                switch (error.constructor) {
                    case HomePageError.Exception:
                        return this.notFound(res, error.errorValue().message)
                    default:
                        return this.fail(res, error.errorValue().message);
                }

            } else {
                const header_footer = result.value.getValue();

                return this.ok<any>(res,header_footer);
            }

        } catch (err) {
            return this.fail(res, err)
        }
    }
}