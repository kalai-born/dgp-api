
import * as express from 'express'
import { BaseController } from "../../shared/infra/http/models/BaseController";
import { FlashDealsService } from '../services/FlashDealsService';
import { HomePageError } from '../errors/HomePageError';
import {logger} from '../../shared/Services/Logging'

export class FlashDealsController extends BaseController {
    private useCase: FlashDealsService;

    constructor(useCase: FlashDealsService) {
        super();
        this.useCase = useCase;
    }

    async executeImpl(req: express.Request, res: express.Response): Promise<any> {

        try {
            const result = await this.useCase.execute(req);

            if (result.isLeft()) {
                const error = result.value;

                switch (error.constructor) {
                    case HomePageError.Exception:
                        return this.notFound(res, error.errorValue().message)
                    default:
                        return this.fail(res, error.errorValue().message);
                }

            } else {
                const header_footer = result.value.getValue();

                return this.ok<any>(res,header_footer);
            }

        } catch (err) {
            logger.error(err);
            return this.fail(res, err);
        }
    }
}