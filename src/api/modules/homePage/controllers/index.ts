import { HeaderFooterController } from "./HeaderFooterController";
import { HeaderFooterService } from "../services/HeaderFooterService";  
import { homePageRepo } from "../repos"
import { FlashDealsController } from "./FlashDealsController";
import { FlashDealsService } from "../services/FlashDealsService";
import { HomePageContentService } from "../services/HomePageContentService";
import { HomePageContentController } from "./HomePageContentController";

let getHeaderFooterUseCase = new HeaderFooterService(homePageRepo);
let getFlashDealsUseCase = new FlashDealsService(homePageRepo);
let getHeaderFooterController = new HeaderFooterController(getHeaderFooterUseCase);
let getFlashDealsController = new FlashDealsController(getFlashDealsUseCase);
let getHomePageContentUseCase = new HomePageContentService(homePageRepo);
let getHomePageContentController = new HomePageContentController(getHomePageContentUseCase)

export {
  getHeaderFooterUseCase,
  getHeaderFooterController,
  getFlashDealsUseCase,
  getFlashDealsController,
  getHomePageContentUseCase,
  getHomePageContentController
}