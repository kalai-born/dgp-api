
import * as express from 'express'
import { BaseController } from "../../shared/infra/http/models/BaseController";
import { CategoryService } from '../services/CategoryService';
import { CLPError } from '../errors/CLPError';

export class CategoryController extends BaseController {
    private useCase: CategoryService;

    constructor(useCase: CategoryService) {
        super();
        this.useCase = useCase;
    }

    async executeImpl(req: express.Request, res: express.Response): Promise<any> {

        try {
            const result = await this.useCase.execute(req);

            if (result.isLeft()) {
                const error = result.value;

                switch (error.constructor) {
                    case CLPError.Exception:
                        return this.notFound(res, error.errorValue().message)
                    default:
                        return this.fail(res, error.errorValue().message);
                }

            } else {
                const categoryService = result.value.getValue();

                return this.ok<any>(res,categoryService);
            }

        } catch (err) {
            return this.fail(res, err)
        }
    }
}