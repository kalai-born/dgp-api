
import * as express from 'express'
import { BaseController } from "../../shared/infra/http/models/BaseController";
import { CLPService } from '../services/CLPService';
import { CLPError } from '../errors/CLPError';

export class CLPController extends BaseController {
    private useCase: CLPService;

    constructor(useCase: CLPService) {
        super();
        this.useCase = useCase;
    }

    async executeImpl(req: express.Request, res: express.Response): Promise<any> {

        try {
            const result = await this.useCase.execute(req);

            if (result.isLeft()) {
                const error = result.value;

                switch (error.constructor) {
                    case CLPError.Exception:
                        return this.notFound(res, error.errorValue().message)
                    default:
                        return this.fail(res, error.errorValue().message);
                }

            } else {
                const clp = result.value.getValue();

                return this.ok<any>(res,clp);
            }

        } catch (err) {
            return this.fail(res, err)
        }
    }
}