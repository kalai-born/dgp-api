import { CLPService } from "../services/CLPService";
import { CLPController } from "./CLPController";
import { clpRepo } from "../repos"
import { CategoryService } from "../services/CategoryService";
import { CategoryController} from "./CategoryController";

let getCLPService = new CLPService(clpRepo);
let getCLPController = new CLPController(getCLPService)
let getCategoryService = new CategoryService(clpRepo)
let getCategoryController = new CategoryController(getCategoryService)

export {
  getCLPController,
  getCategoryController
}