
import express from 'express'
import { getCLPController, getCategoryController } from '../controllers';


const clp = express.Router();
let config: any
let db: any
export default ({ config, db }) => {
  clp.get('/getClpData',
    (req, res) => getCLPController.execute(req, res))

  clp.get('/getCategoryURLPath',
    (req, res) => getCategoryController.execute(req, res))

  return clp;
}

