import { convertToArray, getFormattedResults } from '../../../../lib/util'
import { Service } from '../../shared/core/Service'
import { ICLPLayoutRepo } from '../repos/CLPLayoutRepo';
import { Either, Result, left, right } from "../../shared/core/Result"
import { AppError } from "../../shared/core/AppError";
import { Request } from 'express-serve-static-core';
import express from 'express';
import config from 'config'

const R = require('ramda');
const URL = 'url';
const PAGE_LAYOUT = '/clp';
const includeProductAttr = config['customProduct'] && config['customProduct'].includeAttributes || [];



type Response = Either<
    AppError.UnexpectedError,
    Result<any>
>
export class CLPService implements Service<Request, Promise<any>> {
    private clpRepo: ICLPLayoutRepo;

    constructor(clpRepo: ICLPLayoutRepo) {
        this.clpRepo = clpRepo;
    }

    protected getFormatedCLPMiddleContent = (data) => {
        let middleContent = [];
        try {
          middleContent = convertToArray(data);
        } catch (e) { console.log("\nError: CLP Middle content: ", e) }
        return middleContent;
      };

    protected getFormatedPDPMiddleContent = (data) => {
        let middleContent = [];
        try {
            middleContent = convertToArray(data);
        } catch (e) { console.log("\nError: PDP Middle content: ", e) }
        return middleContent;
    };

    async execute(request: express.Request): Promise<any> {
        try {
            const storeCode = request.query.storeCode.toString();
            let layoutData = await this.clpRepo.getPageLayout(request, storeCode, URL, PAGE_LAYOUT);
            let productData = await this.clpRepo.getProductsByCategory(request,storeCode,['39','43']);
            let middleContent = this.getFormatedCLPMiddleContent((layoutData[0] && layoutData[0].field_nodejson) ? JSON.parse(layoutData[0].field_nodejson) : {});

            for(let p = 0; p < middleContent.length; p++) {
                let element = middleContent[p];
                if (element.name === 'dynamic_product_widgets' && element.value.field_component_token === 'content-product-slider') {
                  middleContent[p].value.productList = productData[0] || [];
                } else if (element.name === 'dynamic_product_widgets' && element.value.field_widget_category === 'menu_product_swiper_slider') {
        
                      let categoryArray = element.value.field_category_list || [];
                      try {
                        for (let i = 0; i < categoryArray.length; i++) {
                          const elementItem = categoryArray[i];
                          let catArray = [];
                          let catId = elementItem.field_category_name && elementItem.field_category_name.field_category_id || '';
                          catArray.push(catId);
                          const productResult = this.clpRepo.getProductsByCategory(request,storeCode,catArray);
                          middleContent[p].value.field_category_list[i].productList = productResult || [];
                        }
                      } catch (e) { console.log("\nError: CLP - Fetching product list for 'menu product swiper slider.", e) }
                }
              }

            return right(Result.ok<any>({ middleContent }));
        } catch (error) {
            return left(new AppError.UnexpectedError(error));
        }
    }
}