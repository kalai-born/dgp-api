import { convertToArray, getFormattedResults } from '../../../../lib/util'
import { Service } from '../../shared/core/Service'
import { ICLPLayoutRepo } from '../repos/CLPLayoutRepo';
import { Either, Result, left, right } from "../../shared/core/Result"
import { AppError } from "../../shared/core/AppError";
import { Request } from 'express-serve-static-core';
import express from 'express';
import config from 'config'

const R = require('ramda');
const URL = 'url';
const PAGE_LAYOUT = '/clp';
const includeProductAttr = config['customProduct'] && config['customProduct'].includeAttributes || [];



type Response = Either<
    AppError.UnexpectedError,
    Result<any>
>
export class CategoryService implements Service<Request, Promise<any>> {
    private clpRepo: ICLPLayoutRepo;

    constructor(clpRepo: ICLPLayoutRepo) {
        this.clpRepo = clpRepo;
    }

    async execute(request: express.Request): Promise<any> {
        try {
            const storeCode = request.query.storeCode.toString();
            const categoryId = request.body.categoryId;
            let batchResults = await this.clpRepo.getCategory(request,storeCode,categoryId)
            return right(Result.ok<any>(batchResults[0].url_path));
        } catch (error) {
            return left(new AppError.UnexpectedError(error));
        }
    }
}