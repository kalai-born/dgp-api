import { ILayoutRepo } from "../../../layout/repos/layout";
import { ICLPLayoutRepo } from "../CLPLayoutRepo"
import { IProductRepo } from "../../../products/repos/products";
import express from "express";
import { getPath } from "../../../../../lib/util";
import { ICategoryRepo } from "../../../category/repos/category"

export class CLPLayout implements ICLPLayoutRepo {
  private layout: ILayoutRepo; 
  private product: IProductRepo;
  private category: ICategoryRepo

  constructor(layout: ILayoutRepo,product:IProductRepo,category:ICategoryRepo) {
    this.layout = layout;
    this.product = product;
    this.category = category
  }

  async getPageLayout(req: express.Request, storeCode: string, field_name: string, pageId: any): Promise<any> {
    let path = getPath(req, 'getPageLayout');
    return this.layout.getPageLayout(path, storeCode, field_name, pageId);
  }

  async getProductsByCategory(req: express.Request, storeCode: string, category_ids: Array<string>): Promise<any> {
    let path = getPath(req, 'getProductsByCategory');
    return this.product.getProductByCategory(storeCode, path, category_ids);
  }

  async getCategory(req: express.Request, storeCode: string, category_ids: Array<string>): Promise<any> {
    let path = getPath(req, 'getCategory');
    return this.category.getCategories(storeCode, path, category_ids);
  }

}