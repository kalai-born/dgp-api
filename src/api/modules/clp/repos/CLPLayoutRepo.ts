import express from "express";


export interface ICLPLayoutRepo {
  getPageLayout(req:express.Request,storeCode: string, field_name:string,pageId:any): Promise<any>;
  getProductsByCategory(req:express.Request,storeCode: string,category_ids: Array<string>): Promise<any>
  getCategory(req:express.Request,storeCode: string,category_ids: Array<string>): Promise<any>
}
