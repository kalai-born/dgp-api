
import { CLPLayout } from "./implementations/CLPElasticSearch";
import { layoutRepo } from "../../layout/repos";
import { productsRepo } from "../../products/repos";
import { categoryRepo } from "../../category/repos";


const clpRepo = new CLPLayout(layoutRepo,productsRepo,categoryRepo);

export { clpRepo }
