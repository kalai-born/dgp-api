import { feApiStatus, feApiError } from '../lib/util';
import { Router } from 'express';
import PlatformFactory from '../platform/factory';
//import { logger } from '../common/logger';
// import { generateJWTToken } from '../common/authutils';
const request = require('request');
const querystring = require('query-string');
//var crypto = require("crypto");
import OAuth from "oauth-1.0a";



export default ({ config, db }) => {
    let socialLoginApi = Router();

    const _getProxy = (req) => {
        const platform = "middleware"
        const factory = new PlatformFactory(config, req)
        return factory.getAdapter(platform, 'user')
      };


    /**
    * GET social system secret keys
    */
    socialLoginApi.get('/socialsystems', (req, res) => {
        try {
            // redisClient.get('socialsystem$$keys', (err, reply) => {
            //     const result = JSON.parse(reply)
            //     if (result && result.result && result.result.length) {
            //         apiStatus(res, result, 200);
            //     } else {
            //         const socialLoginProxy = _getProxy(req)
            //         socialLoginProxy.getSocialSystemsKeys().then((result) => {
            //             redisClient.set('socialsystem$$keys', JSON.stringify(result), 'EX', config.redisCacheExpireInSeconds);
            //             apiStatus(res, result, 200);
            //         }).catch(err => {
            //             logger.error(`method name: socialsystems`, { request: req, error: err });
            //             apiStatus(res, err, 500);
            //         })
            //     }
            // })
            const socialLoginProxy = _getProxy(req)
                    socialLoginProxy.getSocialSystemsKeys().then((result) => {
                        //redisClient.set('socialsystem$$keys', JSON.stringify(result), 'EX', config.redisCacheExpireInSeconds);
                        feApiStatus(res, result, 200);
                    }).catch(err => {
                        //logger.error(`method name: socialsystems`, { request: req, error: err });
                        feApiStatus(res, err, 500);
                    })
        } catch (err) {
            //logger.error(`method name: socialsystems`, { request: req, error: err });
            feApiStatus(res, err, 500);
        }
    });

    /*POST Login through social system*/
    socialLoginApi.post('/login', (req, res) => {
        try {
            const socialLoginProxy = _getProxy(req)
            socialLoginProxy.guestLogin(req.body).then((result) => {
                // logger.info("social login customer token - " + result.customer_access_token)
                // let jwtpayload = {
                //     customertoken: result.customer_access_token
                // }
                // res.req.session = { newrefreshtoken: generateJWTToken(jwtpayload), token: result.customer_access_token }
                feApiStatus(res, result, 200);
            }).catch(err => {
                //logger.error(`method name: social system guest login`, { request: req, error: err });
                feApiError(res, err);
            })
        } catch (err) {
            //logger.error(`method name: social system guest login`, { request: req, error: err });
            feApiError(res, err);
        }
    });

    /*POST Login through social system*/
    socialLoginApi.post('/register', (req, res) => {
        try {
            const socialLoginProxy = _getProxy(req)
            socialLoginProxy.register(req.body).then((result) => {
                feApiStatus(res, result, 200);
            }).catch(err => {
                //logger.error(`method name: social system guest login`, { request: req, error: err });
                feApiError(res, err);
            })
        } catch (err) {
            //logger.error(`method name: social system guest login`, { request: req, error: err });
            feApiError(res, err);
        }

    });

    /*
        POST Login through Twitter Account
    */
    socialLoginApi.post('/loginByTwitter', (req, resp) => {

        // try {
        //     const socialLoginProxy = _getProxy(req)
        //     let twitterKeys = req.body;

        //     let _oauth = OAuth({
        //         consumer: {
        //             key: twitterKeys.key,
        //             secret: twitterKeys.secret
        //         },
        //         signature_method: "HMAC-SHA1",
        //         hash_function: (baseString, key) => {
        //             return crypto
        //                 .createHmac("sha1", key)
        //                 .update(baseString)
        //                 .digest("base64");
        //         }

        //     });
        //     const requestData = {
        //         url: config.twitterAPIs.accessTokenUrl,
        //         method: config.twitterAPIs.accessTokenMethod,
        //         data: {
        //             oauth_token: twitterKeys.oauth_token,
        //             oauth_token_secret: twitterKeys.oauth_token_secret,
        //             oauth_verifier: twitterKeys.oauth_verifier
        //         }
        //     };

        //     logger.info(`method name: Twitter login - API call:` + JSON.stringify(requestData));

        //     request({
        //         url: requestData.url,
        //         method: requestData.method,
        //         form: requestData.data,
        //         headers: _oauth.toHeader(_oauth.authorize(requestData))
        //     }, function (error, response, body) {
        //         if (error) {
        //             logger.error(`method name: Twitter login - API call failed:` + error, { request: req });
        //             apiStatus(resp, error, 500);
        //             return;
        //         } else if (!httpCallSucceeded(response)) {
        //             var errorMessage = errorString(body.message, body.parameters);
        //             logger.error(`method name: Twitter login - API call failed:` + errorMessage, { request: req });
        //             apiStatus(resp, errorMessage, 500);
        //             return;
        //         }

        //         const parsedBody = querystring.parse(body.toString());

        //         const _oauth = OAuth({
        //             consumer: {
        //                 key: twitterKeys.key,
        //                 secret: twitterKeys.secret
        //             },
        //             include_email: true,
        //             signature_method: "HMAC-SHA1",
        //             hash_function: (baseString, key) => {
        //                 return crypto
        //                     .createHmac("sha1", key)
        //                     .update(baseString)
        //                     .digest("base64");
        //             }
        //         });

        //         const token = {
        //             key: parsedBody.oauth_token,
        //             secret: parsedBody.oauth_token_secret
        //         };

        //         const requestObj = {
        //             url: config.twitterAPIs.finalApiUrl,
        //             method: config.twitterAPIs.finalApiMethod,
        //             data: {
        //                 oauth_token_secret: parsedBody.oauth_token_secret
        //             }
        //         };
        //         request({
        //             url: requestObj.url,
        //             method: requestObj.method,
        //             headers: _oauth.toHeader(_oauth.authorize(requestObj, token))
        //         }, (err, res, result) => {
        //             let jsonobject = JSON.parse(result.toString());
        //             let payload = {
        //                 'social_system_id': config.socialsystems.id.twitter,
        //                 'social_id': jsonobject.id,
        //                 'socialToken': parsedBody.oauth_token_secret,
        //                 'firstname': jsonobject.name,
        //                 'email': jsonobject.email
        //             }
        //             socialLoginProxy.guestLogin(payload).then((result) => {
        //                 logger.info("social login customer token - " + result.customer_access_token)
        //                 let jwtpayload = {
        //                     customertoken: result.customer_access_token
        //                 }
        //                 resp.req.session = { newrefreshtoken: generateJWTToken(jwtpayload), token: result.customer_access_token }
        //                 apiStatus(resp, result, 200);
        //             }).catch(err => {
        //                 logger.error(`method name: Twitter login`, { request: req, error: err });
        //                 apiStatus(resp, err, 500);
        //             })
        //         });
        //     });
        // } catch (err) {
        //     logger.error(`method name: Twitter login`, { request: req, error: err });
        // }
    });

    
    /*
        POST Get Twitter Tokens
    */
    socialLoginApi.post('/getTwitterTokens', (req, resp) => {
        // try {
        //     const socialLoginProxy = _getProxy(req)
        //     let twitterKeys = req.body;
        //     let _oauth = OAuth({
        //         consumer: {
        //             key: twitterKeys.key,
        //             secret: twitterKeys.secret
        //         },
        //         signature_method: "HMAC-SHA1",
        //         hash_function: (baseString, key) => {
        //             return crypto
        //                 .createHmac("sha1", key)
        //                 .update(baseString)
        //                 .digest("base64");
        //         }
        //     });

        //     let callbackUrl = config.twitterAPIs.callbackUrl
        //     if (twitterKeys.callbackUrl && twitterKeys.callbackUrl !== '') {
        //       callbackUrl = twitterKeys.callbackUrl
        //     }
        //     const requestData = {
        //       url: config.twitterAPIs.requestTokenUrl,
        //       method: config.twitterAPIs.requestTokenMethod,
        //       data: {
        //         oauth_callback: callbackUrl
        //       }
        //     }

        //     logger.info(`method name: Get Twitter token - API call:` + JSON.stringify(requestData));
        //     request({
        //         url: requestData.url,
        //         method: requestData.method,
        //         form: requestData.data,
        //         headers: _oauth.toHeader(_oauth.authorize(requestData))
        //     }, function (error, response, body) {
        //         if (error) {
        //             console.log('errorrrrrrrrrrrrr',error)
        //             logger.error(`method name: Get Twitter token - API call failed:` + error, { request: req });
        //             apiStatus(resp, error, 500);
        //             return;
        //         } else if (!httpCallSucceeded(response)) {
        //             var errorMessage = errorString(body.message, body.parameters);
        //             console.log('ERRRRRRRRRR"',response)
        //             logger.error(`method name: Get Twitter token - API call failed:` + errorMessage, { request: req });
        //             apiStatus(resp, errorMessage, 500);
        //             return;
        //         }
        //         apiStatus(resp, response, 200);
        //     });
        // } catch (err) {
        //     logger.error(`method name: Get Twitter token`, { request: req, error: err });
        // }
    });

    
    /*
        POST Get Twitter Account Details By Exchanging The Tokens
    */
    socialLoginApi.post('/getTwitterAccountDetails', (req, resp) => {
        // try {
        //     const socialLoginProxy = _getProxy(req)
        //     let twitterKeys = req.body;
        //     let _oauth = OAuth({
        //         consumer: {
        //             key: twitterKeys.key,
        //             secret: twitterKeys.secret
        //         },
        //         signature_method: "HMAC-SHA1",
        //         hash_function: (baseString, key) => {
        //             return crypto
        //                 .createHmac("sha1", key)
        //                 .update(baseString)
        //                 .digest("base64");
        //         }
        //     });

        //     const requestData = {
        //       url: config.twitterAPIs.accessTokenUrl,
        //       method: config.twitterAPIs.accessTokenMethod,
        //       data: {
        //         oauth_token: twitterKeys.exchangeTokens.oauth_token,
        //         oauth_token_secret: twitterKeys.exchangeTokens.oauth_token_secret,
        //         oauth_verifier: twitterKeys.exchangeTokens.oauth_verifier
        //       }
        //     };


        //     logger.info(`method name: Get Twitter Account Details - Exchanging - API call:` + JSON.stringify(requestData));

        //     request({
        //         url: requestData.url,
        //         method: requestData.method,
        //         form: requestData.data,
        //         headers: _oauth.toHeader(_oauth.authorize(requestData))
        //       },

        //       (err, res, data) => {
        //         if (err) {
        //             logger.error(`method name: Get Twitter Account Details - API call failed:` + error, { request: req });
        //             apiStatus(resp, err, 500);
        //             return;
        //         } else if (!httpCallSucceeded(res)) {
        //             var errorMessage = errorString(data.message, data.parameters);
        //             console.log('ERRRRRRRRRR"',res)
        //             logger.error(`method name: Get Twitter Account Details - API call failed:` + errorMessage, { request: req });
        //             apiStatus(resp, errorMessage, 500);
        //             return;
        //         }

        //         const _oauth = OAuth({
        //           consumer: {
        //             key: twitterKeys.key,
        //             secret: twitterKeys.secret
        //           },
        //           include_email: true,
        //           signature_method: 'HMAC-SHA1',
        //           hash_function: (baseString, key) => {
        //             return crypto
        //               .createHmac('sha1', key)
        //               .update(baseString)
        //               .digest('base64');
        //           }
        //         });

        //         const token = {
        //           key: querystring.parse(data.toString()).oauth_token,
        //           secret: querystring.parse(data.toString()).oauth_token_secret
        //         };

        //         const requestData = {
        //           url: config.twitterAPIs.finalApiUrl,
        //           method: config.twitterAPIs.finalApiMethod,
        //           data: {
        //             oauth_token_secret: querystring.parse(data.toString())
        //               .oauth_token_secret
        //           }
        //         };

        //         request({
        //             url: requestData.url,
        //             method: requestData.method,
        //             headers: _oauth.toHeader(_oauth.authorize(requestData, token))
        //           }, function (error, response, body) {
        //             if (error) {
        //                 console.log('errorrrrrrrrrrrrr',error)
        //                 logger.error(`method name: Get Twitter Account Details - API call failed:` + error, { request: req });
        //                 apiStatus(resp, error, 500);
        //                 return;
        //             } else if (!httpCallSucceeded(response)) {
        //                 var errorMessage = errorString(body.message, body.parameters);
        //                 console.log('ERRRRRRRRRR"',response)
        //                 logger.error(`method name: Get Twitter Account Details - API call failed:` + errorMessage, { request: req });
        //                 apiStatus(resp, errorMessage, 500);
        //                 return;
        //             }
        //             apiStatus(resp, response, 200);
        //         });
        //     });
        // } catch (err) {
        //     logger.error(`method name: Get Twitter Account Details`, { request: req, error: err });
        // }
    });

    function httpCallSucceeded(response) {
        return response.statusCode >= 200 && response.statusCode < 300;
    }

    function errorString(message, parameters) {
        if (parameters === null) {
            return message;
        }
        if (parameters instanceof Array) {
            for (var i = 0; i < parameters.length; i++) {
                var parameterPlaceholder = '%' + (i + 1).toString();
                message = message.replace(parameterPlaceholder, parameters[i]);
            }
        } else if (parameters instanceof Object) {
            for (var key in parameters) {
                var parameterPlaceholder = '%' + key;
                message = message.replace(parameterPlaceholder, parameters[key]);
            }
        }

        return message;
    }

    return socialLoginApi
}
