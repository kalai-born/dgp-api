export const doElasticSearch = (esClientObj, searchQueryObj) => {
    return new Promise((resolve, reject) => {
        esClientObj.search({
        ...searchQueryObj
      }, function (error, response, status) {
        if (error) {
          console.log('\nElastic Search error: ' + error);
          return reject(error);
        } else {
          let items = response.body.hits.hits.map(hit => {
            return Object.assign(hit._source, { _score: hit._score });
          });
          return resolve( items );
        }
      });
    });    
};

export const convertToArray = (dataObj, skipList) => {
    let requiredData = skipAttributes(dataObj, skipList);
    let dataArray = [];
    for (let key in requiredData) {
      if (!requiredData.hasOwnProperty(key)) continue;
      let componentName = key.split('--')[0] || '';
      let obj = {
        name: componentName,
        value: requiredData[key]
      }
      dataArray.push(obj);      
    }
    return dataArray;
};

export const skipAttributes = (obj, skipList) => {
    let ourData = {};
    for (let key in obj) {
      if (!obj.hasOwnProperty(key)) continue;
      if (skipList.indexOf(key) === -1) {
        ourData[key] = obj[key];
      }
    }
    return ourData;
};

export const filterProductAttributtes = (productListObj, attrList) => {
  let filteredAttrProductList = [];
  try {
    if(Array.isArray(productListObj) && productListObj.length > 0) {
      productListObj.map((productObj, i) => {
        let filteredObj = {};
        for (const [key, value] of Object.entries(productObj)) {
          if(attrList.indexOf(key) > -1) {
            filteredObj[key] = value;
          }
        }
        filteredAttrProductList.push(filteredObj);
      });
    }
  }
  catch (e) { console.log("\nError while filter product attribute. ", e) }
  return filteredAttrProductList
};
