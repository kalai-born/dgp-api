import { apiStatus, feApiStatus } from '../../../lib/util'
import { Router } from 'express'
const request = require('request')
const md5 = require('md5')

module.exports = ({ config, db }) => {
  let userApi = Router();

  userApi.post('/subscribe', (req, res) => {
    //const client = Magento2Client(multiStoreConfig(config.magento2.api, req));
    let email = req.body.email;
    
    let result = {
      email,
    };

    // if (!email) {
    //   apiStatus(res, 'Invalid e-mail provided!', 500)
    //   return
    // }

    feApiStatus(res, result, 200);
      
  });

  userApi.get('/unsubscribe', (req, res) => {
    //const client = Magento2Client(multiStoreConfig(config.magento2.api, req));
    let email = req.query.email;
    
    let result = {
      email,
    };
    
    if (!email) {
      apiStatus(res, 'Invalid e-mail provided!', 500)
      return
    }

    feApiStatus(res, result, 200);
      
  });

  return userApi
}
