import { apiStatus, feApiStatus } from '../../../lib/util';
import e, { Router } from 'express';
//import PlatformFactory from '../platform/factory';

module.exports = ({config, db}) => {
    let userApi = Router();
    
  /**
   * POST register an user by mobile
   */
 
  userApi.post('/registerByMobile', (req, res) => {

    let userData = req.body;
    //console.log(userData)
    if(!userData.mobile){
      feApiStatus(res, {message: 'MobileNo can not be blank'}, 500);
      return
    }
    else if(!isNaN(userData.mobile)){
      feApiStatus(res, {message: 'Provided value is not a number'}, 500);
      return
    }
    else if(!userData.countryCode){
      feApiStatus(res, {message: 'CountryCode can not be blank'}, 500);
      return
    }else{
      feApiStatus(res, {otpid: '11111'}, 200); 
    }
    
  
  })

  /**
   * POST register an user by email
   */
  userApi.post('/registerByEmail', (req, res) => {

    let userData = req.body;
    let validEmail= function (email) {
      var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/; //for email
      return re.test(email);
    }
    if(!userData.firstName){
      feApiStatus(res, {message: 'Firstname can not be blank'}, 500);
      return
    }
    else if(!(userData.lastName)){
      feApiStatus(res, {message: 'Lastname can not be blank'}, 500);
      return
    }
    else if(!userData.email){
      feApiStatus(res, {message: 'Email can not be blank'}, 500);
      return
    }
    else if (!validEmail(userData.email)) {
      feApiStatus(res, {message: 'Please provide valid email format'}, 500);
      return
    }
    else if(!userData.password){
      feApiStatus(res, {message: 'Password can not be blank'}, 500);
      return
    }
    else{
    feApiStatus(res, {otpid: '11111'}, 200); 
    }

  })

  /**
   * POST validates login user by otp
   */
  userApi.post('/loginWithOtp', (req, res) =>  {
    let userData = req.body;
    let text = userData.otpText;
    let val = text.toString().length;
    if(!userData.otpText){
      feApiStatus(res, {message: 'Otp Text can not be blank'}, 500); 
      return
    }
    else if(userData.otpText != '111111'){
      feApiStatus(res, {message: 'Wrong Otp'}, 500); 
      return
    }
    else if(val != 6){
      feApiStatus(res, {message: 'Invalid Text'}, 500); 
      return
    }
    else if(!userData.otpId){
      feApiStatus(res, {message: 'Otp ID can not be blank'}, 500); 
      return
    }
    else{
    feApiStatus(res, {success: 'login by otp done'}, 200); 
    }
  })
  
  /**
   * POST login user
   */
  userApi.post('/login', (req, res) => {
    let userData = req.body;
   /* let validEmail= function (email) {
      var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/; //for email
      return re.test(email);
    }*/
    if (!userData.username) {
      feApiStatus(res, {message: 'Username can not be blank'}, 500);
      return;
    }
   /* else if (!validEmail(userData.username)) {
      feApiStatus(res, {message: 'Please provide valid email format'}, 500);
      return
    }*/
    else if(userData.password != '1234'){
      feApiStatus(res, {message: 'Invalid password provided!'}, 500);
      return;
    }else{
        feApiStatus(res, {message: 'login successfully'}, 200)
  }
  });
   
  return userApi
};




