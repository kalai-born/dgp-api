var util = require('util');
module.exports = function (restClient) {
    var module = {};
    
    module.subscribe = function (customerData) {
        let payload= {
            data: customerData
        }
        payload.data['language_preference'] = "ae_en"
        return restClient.post('/newsletterSubscribe', payload);
    }

    return module;
}
