'use strict';
import { Client } from '@elastic/elasticsearch';
import * as elastic from '../../lib/elastic'

export default class LandingPageConfig {
  private client: Client;
  private config: any;
  public constructor (app_config) {
    this.config = app_config;
    this.client = elastic.getClient(this.config)
  }

  public getClpConfig (pageType, pageId) {
    return new Promise((resolve, reject) => {
      let match = {};
      match[pageType] = pageId;
      this.client.search({
        index: this.config.wordpress.index,
        type: 'post',
        body: {
          _source: ['meta'],
          query: { match }
        }
      }, (error, response) => {
        if (error) {
          console.log('search error in getClpConfig: ' + error)
          reject(error);
        } else {
          resolve(response.body);
        }
      })
    });
  }

  public getAddressFields (pageType, pageId) {
    return new Promise((resolve, reject) => {
      let match = {};
      match[pageType] = pageId;
      this.client.search({
        index: this.config.wordpress.index,
        type: 'post',
        body: {
          _source: ['meta'],
          query: { match }
        },
        size: 1000
      }, (error, response) => {
        if (error) {
          console.log('search error in getAddressFields: ' + error)
          reject(error);
        } else {
          resolve(response.body);
        }
      })
    });
  }
}
