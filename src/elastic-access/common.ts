import request from 'request';
const { Client } = require('@elastic/elasticsearch')
import * as elastic from '../lib/elastic'

export default class Common {
  private client: any;
  private config: any;

  public constructor (app_config) {
    this.config = app_config;
    this.client = elastic.getClient(app_config)
  }
  // Query elastic search for VSF only
  public queryElasticSearch (elasticBackendUrl, httpMethod, query, auth) {
    return new Promise((resolve, reject) => {
      request({ // do the elasticsearch request
        uri: elasticBackendUrl,
        method: httpMethod,
        body: query,
        json: true,
        auth: auth
      }, (_err, _res, _resBody) => {
        if (_err) reject(_err);
        resolve(_resBody);
      });
    });
  }
}
