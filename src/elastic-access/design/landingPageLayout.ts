'use strict';
import * as elastic from '../../lib/elastic';
import Task = require("folktale/concurrency/task");
const task = Task.task
const R = require('ramda');
import {getFormattedResults} from'../../lib/util';
import HomePageData from'../data/homePage';

const HEADER_MENU = 'header_menu';
const FOOTER_MENU = 'footer_menu'
const HEADER = 'header'
const FOOTER = 'footer'
const HOME_PAGE_COMPONENTS = 'homePageComponents'


const STORE_CODE = 'storeCode';
const STORE_VIEWS = 'storeViews';
const ELASTIC_SEARCH = 'elasticsearch';
const DRUPAL = 'drupal';
const INDEX = 'index';
const DOC = '_doc';
const URL = 'url';
const FIELD_MC_NAME = 'field_machine_name';

export default class LandingPageLayout {
  private client: any;
  private config: any;
  private landingPageData: any
  public constructor (app_config) {
    this.config = app_config;
    this.client = elastic.getClient(app_config)
  }

  // Get CMS page design from Elastic search

  public getPageLayout =  (context) => task((resolver) => {
    try {
      const { storeCode, pageName } = context || {};

      !storeCode ? resolver.reject('Store code not provided.') : '';
      !pageName ? resolver.reject('Page name not provided.') : '';

      const queryObj = {
        index: R.path([STORE_VIEWS, storeCode, ELASTIC_SEARCH, DRUPAL, INDEX],this.config),
        type: DOC,
        body: {
          query: {
            query_string: {
              fields: [URL],
              query: pageName
            }
          }
        }
      };
      this.client.search(queryObj,(err,response)=>{
        err?resolver.reject(err):resolver.resolve(response)
      })
      
    } catch (error) {
      resolver.reject(error)
    }

  })  

}
