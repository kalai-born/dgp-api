'use strict';
import { Client } from '@elastic/elasticsearch';
import * as elastic from '../../lib/elastic'
import Task = require("folktale/concurrency/task");
const task = Task.task
const R = require('ramda');
import {getFormattedResults} from '../../lib/util'
import { Worker } from 'worker_threads';
//import {query} from '../../lib/constants';
let homePageData = ''




export default class HomePageData {
  private client: Client;
  private config: any;

  public constructor (app_config) {
      this.config = app_config;
      this.client = elastic.getClient(this.config)

  }

  private dealList = (context) => task((resolver)=>{
       const includeProductAttr = this.config.customProduct && this.config.customProduct.includeAttributes || [];
       let productList = R.prop('productList',context)
       let pl = productList?productList:[]
       let skuList  = R.map((x)=>R.prop('product_sku',x),pl)
       let productQuery = {
        index: this.config.carousel.productIndex,
        type: this.config.carousel.productType,
        body: {
          size: this.config.carousel.maxProduct,
          query: {
            terms: { 'sku': skuList }
          },
          _source: includeProductAttr
        }            

       }
       this.client.search(productQuery,(err,response)=>{                  
         err?resolver.reject(err):resolver.resolve(R.assoc('productDealsInfo',getFormattedResults(response),context))
       })
  })
  private getFlashdeals =  (context) => task((resolver) => {
      const includeProductAttr = this.config.customProduct && this.config.customProduct.includeAttributes || [];
      try {
        let queryObj = {
          index: this.config.carousel.flashDeals.index,
          type: this.config.carousel.flashDeals.type,
          body: {
            size: this.config.carousel.flashDeals.maxProduct
          }
        };
        this.client.search(queryObj,(err,response)=>{
          err?resolver.reject(err):resolver.resolve(R.assoc('productList',getFormattedResults(response),{}))
        })
 
 
      } catch (error) {
        resolver.reject(error)
      }
    
  })

  private execute_search = (context) => task((resolver) => {
      let key = R.take(1)(R.keys(context))
      this.client.search(R.prop(key,context),(err,result)=>{
        err?resolver.reject(err):resolver.resolve(R.assoc(key,getFormattedResults(result,['_source']),context))
      })

  })
  

  private getProducts = (context) => task((resolver)=>{
      let queries = R.prop('queries',context)
      let menus = R.keys(queries)
      let queryTasks =menus.map((menu)=>{
          return this.execute_search(R.assoc(menu,R.path([menu,'query'],queries),{}))
      })
      const query_promises = queryTasks.map((job) => {
        return new Promise((res, rej) => {
          job.run()
            .listen({
              onRejected: (r) => rej(r),
              onResolved: (x) => res(x)
            })
        })
      })
      Promise.all(query_promises).then((results)=>{
        resolver.resolve(R.assoc('results',R.mergeAll(results),context))
      }).catch((err)=>{
        resolver.reject(err)
      })
  })

  private getProductSearchQuery = (context) => task((resolver)=>{
      let skuList = R.prop('skuList',context)
      let getQuery =(accum,curr)=>{
        return R.compose(    
           R.assocPath([curr,'query','body','query','terms','sku'],R.prop(curr,skuList)),
           R.assocPath([curr,'query','body','size'],R.path(['headerMenus',curr,'maxProduct'],this.config)),
           R.assocPath([curr,'query','type'],R.path(['headerMenus',curr,'type'],this.config)),
           R.assocPath([curr,'query','index'],R.path(['headerMenus',curr,'index'],this.config)),
        )(accum)
      }
      let queryList = R.reduce(getQuery,{},R.keys(skuList))
      resolver.resolve(R.assoc('queries',queryList,context))
      
  })

  private getSkuList = (context) => task((resolver)=>{
    let header_menus = R.pathOr({},['header','headerMenus'],context);
    let getSkus = R.compose(
      R.assoc('topDeals',R.map((x) => R.prop('value',x),R.pathOr([],['topDeals','skus','field_product_sku'],header_menus))),
      R.assoc('latestArrivals',R.map((x)=> R.prop('value',x),R.pathOr([],['latestArrivals','skus','field_product_sku'],header_menus))),
      R.assoc('bestSellers',R.map((x)=> R.prop('value',x),R.pathOr([],['bestSellers','skus','field_product_sku'],header_menus)))
    )({})
    resolver.resolve(R.assoc('skuList',getSkus,context));  

  })

  public getfd = R.composeK(
    this.dealList,
    this.getFlashdeals
  )
  
  public getProductList = R.composeK(
    this.getProducts,
    this.getProductSearchQuery,
    this.getSkuList
  )

}
