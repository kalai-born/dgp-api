'use strict';
import { Client } from '@elastic/elasticsearch';
import elastic from '../../lib/elastic';

export default class UrlRedirect {
  private client: any;
  private config: any;
  public constructor (app_config) {
    this.client = elastic.getClient(app_config)
    this.config = app_config;
  }

  public queryUrlRedirectData (data, index) {
    try {
       let responseData = [];
     
      return new Promise((resolve, reject) => {
        this.client.search({
          index: index,
          type: data.type,
          body: {
            query: data.query
          },
          size: 1000
        }, (error, response, status) => {
          if (error) {
            reject(error);
          } else {
            if (response.body.hits.hits.length) {
              responseData = response.body.hits.hits.map(el => el._source)
              // just dump it to the browser, result = JSON object
              resolve(responseData);
            }else{
              resolve(responseData);
            }
          }
        });
      });
    } catch (error) {
      console.log(JSON.stringify(error))
    }
  }

}
