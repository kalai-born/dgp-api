import { multiStoreConfig } from './util'

class NewsletterProxy{
  constructor (config, req) {
    const Magento2Client = require('../../magento2-rest-client').Magento2Client;
    this.api = Magento2Client(multiStoreConfig(config.magento2.api, req));
  }

  subscribe (userData) {
    return this.api.newsletter.subscribe(userData)
  }
}

module.exports = NewsletterProxy
