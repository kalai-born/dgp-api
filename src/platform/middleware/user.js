import AbstractUserProxy from '../abstract/user'
import { multiStoreConfig } from './util'

class UserProxy extends AbstractUserProxy {
  constructor (config, req) {
    const MiddlewareClient = require('../../middileware-rest-client').MiddlewareClient;
    super(config, req)
    this.api = MiddlewareClient(multiStoreConfig(config.magento2.api, req));
  }
  login (userData) {
 	  return this.api.users.login(userData)
  }
  loginWithOTP (userData) {
 	  return this.api.users.loginWithOTP(userData)
  }
  registerByMobile (userInfo) {
    return this.api.users.registerByMobile(userInfo)
  }
  registerByEmail (userInfo) {
    return this.api.users.registerByEmail(userInfo)
  }
  generateLoginOTP (userInfo) {
    return this.api.users.generateLoginOTP(userInfo)
  }




  //social login section
  guestLogin (userInfo) {
    return this.api.users.guestLogin(userInfo)
  }
  register (userInfo) {
    return this.api.users.register(userInfo)
  }
  getSocialSystemsKeys() {
    return this.api.users.socialSystemsKeys()
  }
}

module.exports = UserProxy
