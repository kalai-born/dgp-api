'use strict';

var RestClient = require('./lib/rest_client').RestClient;
var users = require('./lib/users');


module.exports.MiddlewareClient = function (options, apiversion) {
  var instance = {
    addMethods (key, module) {
      var client = RestClient(options);
      if (module) {
        if (this[key]) { this[key] = Object.assign(this[key], module(client)) } else { this[key] = module(client) }
      }
    }
  };
  var client = RestClient(options);
  instance.users = users(client);
  
  return instance;
}
