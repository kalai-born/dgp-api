var util = require('util');
let userArray = [{firstName: 'naresh', lastName: 'durga', email: 'naresh.durga@borngroup.com', mobile: '9941677108', password: 'Hellon1!', isActive: true}];
import config from 'config'
module.exports = function (restClient) {
    var module = {};
    module.login = function (loginDetails) {
        let payload = {
          data: loginDetails
        }
        // return restClient.post('/customer/token', payload);
        return login(loginDetails)
    }
    module.registerByMobile = function (userDetails) {
        let payload = {
          data: userDetails
        }
        // return restClient.post('/customer/token', payload);
        return registerByMobile(userDetails)
    }
	module.registerByEmail = function (userDetails) {
        let payload = {
          data: userDetails
        }
        // return restClient.post('/customer/token', payload);
        return registerByEmail(userDetails)
    }
    module.loginWithOTP = function (loginDetails) {
        let payload = {
          data: loginDetails
        }
        // return restClient.post('/customer/token', payload);
        return loginWithOTP(loginDetails)
    }

    module.generateLoginOTP = function (userDetails) {
        let payload = {
          data: userDetails
        }
        // return restClient.post('/customer/token', payload);
        return generateLoginOTP(userDetails)
    }

    module.socialSystemsKeys = function () {
        return socialSystemsKeys()
    }




    //Social login functionality start

    module.guestLogin = function (userDetails) {
        let payload = {
          data: userDetails
        }
        // return restClient.post('/customer/token', payload);
        return guestLogin(userDetails)
    }

    module.register = function (userDetails) {
        let payload = {
          data: userDetails
        }
        // return restClient.post('/customer/token', payload);
        return register(userDetails)
    }

    return module;
}


function login (loginDetails) {
    if (loginDetails.username) {
    	const userExist = userArray.some(el => ((el.email === loginDetails.username || el.mobile === loginDetails.username) && el.password === loginDetails.password && el.isActive));
		if (userExist) {
			let updatedUserInfo = {}
			userArray.forEach (function(user){
			  	if ((user.email === loginDetails.username || user.mobile === loginDetails.username) && user.password === loginDetails.password && user.isActive) {
			  		updatedUserInfo['mobile'] =  user.mobile ? user.mobile.replace(/\s+/g, '') : null;
			  		updatedUserInfo['countryCode'] = user.countryCode
			  		updatedUserInfo['firstName'] = user.firstName
			  		updatedUserInfo['lastName'] = user.lastName
			  		updatedUserInfo['email'] = user.email
			  	}
			})
			return new Promise((resolve, reject) => {
		      return resolve({
				    "status": true,
				    "message": "CUSTOMER_TOKEN_GENERATED",
				    "result": [
				      {
				        "access_token": "4i1jju0xembkmrmutdjd41n16u7mdptf",
				        "userInfo": updatedUserInfo
				      }
				    ],
				    "code": "CUSTOMER_TOKEN_GENERATED"
				});
		    });
		}else{
			return new Promise((resolve, reject) => {
		      	return resolve({
			        "status": false,
			        "message": "The Email Address/ Mobile Number or Password entered is not correct",
			        "result": [
			            {
			                "invalid_customer": true
			            }
			        ],
			        "code": "CUSTOMER_DOES_NOT_EXIST"
			    });
		    });
		}
    }
}

function loginWithOTP (loginDetails) {
    if (loginDetails.otpId && loginDetails.otpText != '123456') {
    	return new Promise((resolve, reject) => {
	      	return resolve({
		        "status": false,
		        "message": "OTP is incorrect",
		        "result": [
		            {
		                "invalid_otp": true
		            }
		        ],
		        "code": "INVALID_OTP"
			});
	    });
	    return
    }else{
    	const userExist = userArray.some(el => (el.otpId === loginDetails.otpId && el.otpText === loginDetails.otpText));
		if (userExist) {
			let updatedUserInfo = {}
			userArray.forEach (function(user){
			  	if (user.otpId === loginDetails.otpId && user.otpText === loginDetails.otpText) {
			  		user.isActive = true
			  		updatedUserInfo['mobile'] =  user.mobile ? user.mobile.replace(/\s+/g, '') : null;
			  		updatedUserInfo['countryCode'] = user.countryCode
			  		updatedUserInfo['firstName'] = user.firstName
			  		updatedUserInfo['lastName'] = user.lastName
			  		updatedUserInfo['email'] = user.email
			  	}
			})
			return new Promise((resolve, reject) => {
		      return resolve({
				    "status": true,
				    "message": "CUSTOMER_TOKEN_GENERATED",
				    "result": [
				      {
				        "access_token": "4i1jju0xembkmrmutdjd41n16u7mdptf",
				        "userInfo": updatedUserInfo
				      }
				    ],
				    "code": "CUSTOMER_TOKEN_GENERATED"
				});
		    });
		}else{
			return new Promise((resolve, reject) => {
		      	return resolve({
			        "status": false,
			        "message": "The Email Address/ Mobile Number entered is not correct",
			        "result": [
			            {
			                "invalid_customer": true
			            }
			        ],
			        "code": "CUSTOMER_DOES_NOT_EXIST"
			    });
		    });
		}
    }
}
function registerByMobile(userDetails){
	if (userDetails.mobile) {
		const userExist = userArray.some(el => el.mobile === userDetails.mobile);
		if (!userExist) {
			let otpID = Math.floor(1000 + Math.random() * 9000)
			userDetails['isActive'] = false
			userDetails['otpId'] = otpID
			userDetails['otpText'] = '123456'
			userArray.push(userDetails)
			return new Promise((resolve, reject) => {
		      return resolve({
			    "status": true,
			    "message": "OTP_ID_GENERATED",
			    "result": [
			      {
			        "otpID": otpID
			      }
			    ],
			    "code": "OTP_ID_GENERATED"
			  });
	    	});
		}else{
			return new Promise((resolve, reject) => {
		      	return resolve({
			        "status": false,
			        "message": "There is already an account existing with this mobile number. Please register with a different mobile number or login to Sharaf DG website using this mobile number or email address associated with this account",
			        "result": [
			            {
			                "mobile_already_exist": true
			            }
			        ],
			        "code": "There is already an account existing with this mobile number. Please register with a different mobile number or login to Sharaf DG website using this mobile number or email address associated with this account"
			    });
		    });
		}
	}
}

function registerByEmail(userDetails){
	if (userDetails.email) {
		const userExist = userArray.some(el => el.email === userDetails.email);
		if (!userExist) {
			let otpID = Math.floor(1000 + Math.random() * 9000)
			userDetails['isActive'] = false
			userDetails['otpId'] = otpID
			userDetails['otpText'] = '123456'
			userArray.push(userDetails)
			return new Promise((resolve, reject) => {
		      return resolve({
				    "status": true,
				    "message": "OTP_ID_GENERATED",
				    "result": [
				      {
				        "otpID": otpID
				      }
				    ],
				    "code": "OTP_ID_GENERATED"
				});
		    });
		}else{
			return new Promise((resolve, reject) => {
		      	return resolve({
			        "status": false,
			        "message": "There is already an account existing with this email address. Please register with a different email address or login to Sharaf DG using this email address",
			        "result": [
			            {
			                "email_already_exist": true
			            }
			        ],
			        "code": "email number already exist"
			    });
		    });
		}
	}
}

function generateLoginOTP(userDetails){
	if (userDetails.username) {
		const userExist = userArray.some(el => (el.mobile === userDetails.username || el.email === userDetails.username));
		if (!userExist) {
			return new Promise((resolve, reject) => {
		      	return resolve({
			        "status": false,
			        "message": "The Email Address/ Mobile Number entered is not correct",
			        "result": [
			            {
			                "invalid_customer": true
			            }
			        ],
			        "code": "customer_does_not_exist"
			    });
		    });
		}else{
			let otpID = Math.floor(1000 + Math.random() * 9000)
			userArray.forEach (function(user){
			  	if ((user.mobile === userDetails.username || user.email === userDetails.username)) {
			  		user.otpId = otpID
			  		user.otpText = '123456'
			  	}
			})
			return new Promise((resolve, reject) => {
		      return resolve({
			    "status": true,
			    "message": "OTP_ID_GENERATED",
			    "result": [
			      {
			        "otpID": otpID
			      }
			    ],
			    "code": "OTP_ID_GENERATED"
			  });
	    });
		}
	}
}

function activateAccount(updateInfo) {
  userArray.forEach (function(user){
  	if (user.otpId === updateInfo.otpId && user.otpText === updateInfo.otpText) {
  		user.isActive = true
  	}
  	return user;
  })
}


//Social login started

function guestLogin(userDetails){
	if (userDetails.social_id) {
		const isActiveUser = userArray.some(el => ((userDetails.email && el.email === userDetails.email) || (userDetails.mobile && el.mobile === userDetails.mobile)) && el.isActive);
		if (!isActiveUser) {
			let customer_info = {}
			const existingUser = userArray.some(el => ((userDetails.email && el.email === userDetails.email) || (userDetails.mobile && el.mobile === userDetails.mobile)));
			if (!existingUser) {
				let customerId = userDetails.social_id.toString()+'_'+Math.floor(1000 + Math.random() * 9000).toString()
				userDetails['isActive'] = false
				userDetails['customerId'] = customerId
				userArray.push(userDetails)
			}
			userArray.forEach (function(user){
			  	if((userDetails.email && user.email === userDetails.email) || (userDetails.mobile && user.mobile === userDetails.mobile)) {
			  		customer_info['isActive'] = user.isActive
			  		customer_info['mobile'] =  user.mobile ? user.mobile.replace(/\s+/g, '') : null;
			  		customer_info['countryCode'] = user.countryCode
			  		customer_info['firstName'] = user.firstName
			  		customer_info['lastName'] = user.lastName
			  		customer_info['email'] = user.email
			  		customer_info['customerId'] = user.customerId
			  		customer_info['social_system_id'] = user.social_system_id
			  		customer_info['social_id'] = user.social_id
			  	}
			})

			return new Promise((resolve, reject) => {
		      return resolve({
			    "status": true,
			    "message": "Guest customer created",
			    "result": [
			      {
			        "userInfo": customer_info
			      }
			    ],
			    "code": "Guest_customer_created"
			  });
	    	});
		}else {
			let customer_info = {}
			userArray.forEach (function(user){
			  	if((userDetails.email && user.email === userDetails.email) || (userDetails.mobile && user.mobile === userDetails.mobile)) {
			  		user.isActive = true
			  		customer_info['mobile'] =  user.mobile ? user.mobile.replace(/\s+/g, '') : null;
			  		customer_info['countryCode'] = user.countryCode
			  		customer_info['firstName'] = user.firstName
			  		customer_info['lastName'] = user.lastName
			  		customer_info['email'] = user.email
			  		customer_info['customerId'] = user.customerId
			  		customer_info['social_system_id'] = user.social_system_id
			  		customer_info['social_id'] = user.social_id
			  	}
			})
			
			return new Promise((resolve, reject) => {
		      return resolve({
				    "status": true,
				    "message": "CUSTOMER_TOKEN_GENERATED",
				    "result": [
				      {
				        "access_token": "4i1jju0xembkmrmutdjd41n16u7mdptf",
				        "userInfo": customer_info
				      }
				    ],
				    "code": "CUSTOMER_TOKEN_GENERATED"
				});
		    });
		}
	}
}


function register(userDetails){
	console.log("userDetails")
	console.log(userDetails)
	if (userDetails.social_id) {
		const existingUser = userArray.some(el => (((userDetails.email && el.email === userDetails.email) || (userDetails.mobile && el.mobile === userDetails.mobile)) && el.customerId === userDetails.customerId));
		console.log(existingUser)
		if (existingUser) {
			let updatedUserInfo = {}
			userArray.forEach (function(user){
			  	if (user.email === userDetails.email || user.mobile === userDetails.mobile) {
			  		user.isActive = true
					user["firstName"] = userDetails.firstName
			  		user["lastName"] = userDetails.lastName
			  		user["mobile"] = userDetails.mobile
			  		user["countryCode"] = userDetails.countryCode
			  		updatedUserInfo['mobile'] =  user.mobile ? user.mobile.replace(/\s+/g, '') : null;
			  		updatedUserInfo['countryCode'] = user.countryCode
			  		updatedUserInfo['firstName'] = user.firstName
			  		updatedUserInfo['lastName'] = user.lastName
			  		updatedUserInfo['email'] = user.email
			  	}
			})

			return new Promise((resolve, reject) => {
		      return resolve({
				    "status": true,
				    "message": "CUSTOMER_TOKEN_GENERATED",
				    "result": [
				      {
				        "access_token": "4i1jju0xembkmrmutdjd41n16u7mdptf",
				        "userInfo": updatedUserInfo
				      }
				    ],
				    "code": "CUSTOMER_TOKEN_GENERATED"
				});
		    });
		}else{
			return new Promise((resolve, reject) => {
		      	return resolve({
			        "status": false,
			        "message": "The Email Address/ Mobile Number or Password entered is not correct",
			        "result": [
			            {
			                "invalid_customer": true
			            }
			        ],
			        "code": "CUSTOMER_DOES_NOT_EXIST"
			    });
		    });
		}
	}
}


function socialSystemsKeys(){
	return new Promise((resolve, reject) => {
		return resolve({
		    "status": true,
		    "message": "SOCIAL_SYSTEM_SUCCESS",
		    "result": config.social_keys,
		    "code": "SOCIAL_SYSTEM_SUCCESS"
		});
	});
}