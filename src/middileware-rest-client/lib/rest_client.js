'use strict';

var OAuth = require('oauth-1.0a');
var request = require('request');
const crypto = require('crypto');
var humps = require('humps');
var sprintf = require('util').format;
import { v4 as uuidv4 } from 'uuid';
import config from 'config'

//import { logger } from '../../common/logger';

module.exports.RestClient = function (options) {
    var instance = {};

    var servelrUrl = options.url;
    var apiVersion = options.version;
    var storeCode = options.storeCode;


    function hash_function_sha1(base_string, key) {
        return crypto
            .createHmac('sha1', key)
            .update(base_string)
            .digest('base64')
    }

    var oauth = OAuth({
        consumer: {
            key: options.consumerKey,
            secret: options.consumerSecret
        },
        signature_method: 'HMAC-SHA1',
        hash_function: hash_function_sha1,
    });
    var token = {
        key: options.accessToken,
        secret: options.accessTokenSecret
    };

    function apiCall(request_data, request_token = '') {

        var payload = request_data.body || {} ;
        const txid = uuidv4();
        payload.app_context =
        {
            application_id: config.applicationId,
            transaction_id: txid,
            store_code: storeCode || config.defaultStoreCode
        }
        let reqtimestamp = new Date().getTime()
        /* eslint no-undef: off*/
        return new Promise(function (resolve, reject) {
            request({
                url: request_data.url,
                method: request_data.method,
                headers: request_token ? { 'Authorization': 'Bearer ' + request_token } : oauth.toHeader(oauth.authorize(request_data, token)),
                json: true,
                body: payload,
            }, function (error, response, body) {
                if (error) {
                    //logger.error('Error occured: - ' + 'Duration - ' + ((new Date().getTime()  - reqtimestamp)/1000) ,{ error: error, storeCode: storeCode, transactionId: txid, url: request_data.url + "| Type: " +  request_data.method + "| payload >>>>" +  JSON.stringify(payload)});
                    console.log('error', payload);
                    reject(error);
                    return;
                } else if (!httpCallSucceeded(response)) {
                    var errorMessage = 'HTTP ERROR ' + response.code;
                    if (body && body.hasOwnProperty('message'))
                        errorMessage = errorString(body.message, body.hasOwnProperty('parameters') ? body.parameters : {});
                        console.log('error', payload);
                    //logger.error('API call failed: Duration - ' + ((new Date().getTime()  - reqtimestamp)/1000)  + ' Response - ' + JSON.stringify(response), { storeCode: storeCode, transactionId: txid, url: request_data.url + "| Type: " +  request_data.method  + "| payload >>>>" +  JSON.stringify(payload)});
                    reject({
                        errorMessage,
                        code: response.statusCode,
                        toString: () => {
                            return this.errorMessage
                        }
                    });
                }
                //                var bodyCamelized = humps.camelizeKeys(body);
                //                resolve(bodyCamelized);
                //logger.info('API call successfull - ' + 'Duration - ' + ((new Date().getTime()  - reqtimestamp)/1000), { storeCode: storeCode, transactionId: txid, url: request_data.url + "| Type: " +  request_data.method  + JSON.stringify(payload) + "| Response >>>>" + JSON.stringify(body)});
                resolve(body);
            });
        });
    }

    instance.consumerToken = function (login_data) {
        return apiCall({
            url: createUrl('/integration/customer/token'),
            method: 'POST',
            body: login_data
        })
    }

    function httpCallSucceeded(response) {
        return response.statusCode >= 200 && response.statusCode < 300;
    }

    function errorString(message, parameters) {
        if (parameters === null) {
            return message;
        }
        if (parameters instanceof Array) {
            for (var i = 0; i < parameters.length; i++) {
                var parameterPlaceholder = '%' + (i + 1).toString();
                message = message.replace(parameterPlaceholder, parameters[i]);
            }
        } else if (parameters instanceof Object) {
            for (var key in parameters) {
                var parameterPlaceholder = '%' + key;
                message = message.replace(parameterPlaceholder, parameters[key]);
            }
        }

        return message;
    }

    instance.get = function (resourceUrl, request_token = '') {
        var request_data = {
            url: createUrl(resourceUrl),
            method: 'GET'
        };
        return apiCall(request_data, request_token);
    }

    function createUrl(resourceUrl) {
        return servelrUrl + '/' + apiVersion + resourceUrl;
    }

    instance.post = function (resourceUrl, data, request_token = '') {
        var request_data = {
            url: createUrl(resourceUrl),
            method: 'POST',
            body: data
        };
        return apiCall(request_data, request_token);
    }

    instance.put = function (resourceUrl, data, request_token = '') {
        var request_data = {
            url: createUrl(resourceUrl),
            method: 'PUT',
            body: data
        };
        return apiCall(request_data, request_token);
    }

    instance.delete = function (resourceUrl, request_token = '') {
        var request_data = {
            url: createUrl(resourceUrl),
            method: 'DELETE'
        };
        return apiCall(request_data, request_token);
    }
    instance.deleteWithBody = function (resourceUrl, request_token = '', data = null) {
      var request_data = {
        url: createUrl(resourceUrl),
        method: 'DELETE',
        body: data
    };
    return apiCall(request_data, request_token);
  }

    return instance;
}
